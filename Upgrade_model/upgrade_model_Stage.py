import argparse
import requests
import os
 
#*args Model From CLI 

# Start Parser 

parser = argparse.ArgumentParser(description="Ready to parse")
parser.add_argument('model', action='store', help= 'Tag a model as active on production server')
arguments = parser.parse_args()
print(f"Model name is {arguments.model}")  

# End  Start Parser 

# SET HEADER
header = {'Content-Type': 'application/json'}
body = {'username': 'me', 'password': os.environ['RASA_PASSWORD']}
full_adress=os.environ['RASA_DNS']
req = requests.post(full_adress+'/api/auth', json=body, headers=header)
response = req.json()
print("Access token response: ", response)
bearer_token = response["access_token"]
project_id = 'default'
tag = 'production'
model = arguments.model
# END SET HEADER
if bearer_token is not None:
    header = {'Authorization': "Bearer " + bearer_token,
              'Content-Type': 'application/json'}
    print("the URL string : ", (full_adress+f"/api/projects/{project_id}/models/{model}/tags/{tag}"), '\n')
    req = requests.put( (full_adress+f"/api/projects/{project_id}/models/{model}/tags/{tag}"), headers=header)
    print(req.text, type(req.text), len(req.text))
    if len(req.text) == 0:
        print(str(req.text), "the actual PUT Request")
        print("Model Activated")
else:
    print("Bearer token not retrieved")
