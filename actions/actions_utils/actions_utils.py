from actions.DAO_Chatbot.Entity.Finance_entity import Finance_statistic_entity
from rasa_sdk.types import DomainDict
from actions.DAO_Chatbot.Entity.Simulations_entities import Credit_simulation, Deposit_simulation, simulation
from typing import Any, Optional, Text, Dict, List
import pandas as pd, datetime
from rasa_sdk import Action, Tracker
from actions.DAO_Chatbot.Entity.Chatbot_info_entity import chatbot_info_entity
from rasa_sdk.events import ActionExecuted, ConversationPaused, FollowupAction, SlotSet, EventType, SessionStarted, ReminderScheduled, BotUttered, UserUtteranceReverted,ActiveLoop
from actions.DAO_Chatbot.Implementation.ImpChatbot_offer import Implchatbot_offer
from rasa_sdk.executor import CollectingDispatcher
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions.return_exception import Empty_list_exception, Note_found_exception
from rasa_sdk.interfaces import ActionExecutionRejection
from actions.Helpers.Convert_helpers import Convert_string_datetime, convert_class_offers_to_json, convert_type_to_button, \
    convert_types_to_buttons, Convert_VariableName_to_String, convert_item_id_to_buttons,convert_data_to_rocketchatAttachment
from actions.Helpers.Extract_helpers import Extract_Simulation_types_list, Extract_offers_types_list, Extract_id_string
from actions.DAO_Chatbot.Implementation import ImpChatbot_info
from actions.DAO_Chatbot.Implementation.ImpSimulation_file import Imp_Simulation_credit, Impl_Simulations_Deposit
from actions.Helpers.Credits_calcul import calcul_credit_monthly, calcul_credit_yearly
from rasa_sdk.forms import FormAction, FormValidationAction, REQUESTED_SLOT
from actions.Helpers.Validators_helpers import Is_Valid_SameCurrency, Is_Validate_intervale, Is_valide_RocketChat_Channel, Is_valid_attempt
from actions.Cache.simulations_cache import Get_Current_selected_simulation_name_list, Get_calcul_Deposit_simulation, Get_selected_simulation, \
    Get_Calcul_Credit_simulation, Get_simulations_subtypes, cancel_asking_onSlot_element, cancel_simulation, reset_form_slots
from actions.DAO_Chatbot.Implementation.ImpAgency_location import Imp_Agency_location   
from actions.GoogleMap.entitiesMap import client_cordinates  
from actions.GoogleMap.Methodes import  Get_nearest_location
from rasa.core.channels.rocketchat import     RocketChatInput
from actions.Translater.Translate_message.Translate_utter_message import Translate_message_to_other_language
import datetime
from actions.DAO_Chatbot.Implementation.ImpFinance_currency_Grpc import Imp_finance_Curreny
import random


class Action_afirm_deny(Action):
    def name(self)->Text:
        return "action_affirm_deny"
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        buttons = [{'title': 'yes', 'payload': '/affirm'}, {'title': 'No', 'payload': '/deny'}]
        
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            RocketChat_Message=convert_data_to_rocketchatAttachment("buttons",attachment_data=buttons)
            dispatcher.utter_message(text="Pleaze confirm this operation",attachment=RocketChat_Message)
            
        else:
            dispatcher.utter_message(text="Pleaze confirm this operation",buttons=buttons)
        return []
    
class Action_Confirm(Action):
    def name(self)->Text:
        return "action_ask_confirm"
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        buttons = [{'title': 'yes', 'payload': '/affirm'}, {'title': 'No', 'payload': '/deny'}]
        
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            RocketChat_Message=convert_data_to_rocketchatAttachment("buttons",attachment_data=buttons)
            dispatcher.utter_message(text="to reach an agent 🧑‍🔧 you must confirm (Yes or No) this operation",attachment=RocketChat_Message)
            
            
        else:
            dispatcher.utter_message(text="Pleaze confirm this operation",buttons=buttons)
        return []    
class Action_Reset_offers_form(Action):
    def name(self)->Text:
        return "action_reset_offer_form"
    slots_names=["selected_type","selectedofferdeatils"]
    
    async def run (self, dispatcher, tracker: Tracker, domain: "DomainDict")-> List[Dict[Text, Any]]:

        Reset_Events=reset_form_slots(self.slots_names)
        return Reset_Events


class Ation_Reset_form_currrency(Action):
    def name(self)->Text:
        return "action_reset_form_currency"     
    slots_names=["tocurrency","fromcurrency"]
           
    async def run(self, dispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        Reset_Events=reset_form_slots(self.slots_names)
        
        return Reset_Events       
class Ation_Reset_form_currrency_statistc(Action):
    def name(self)->Text:
        return "action_reset_form_currency_statistic"                
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        slots_names=["tocurrency","fromcurrency","DateCurrencyType","ColumnsCurrencyExchange","intervalduration","elementlist"]
        Reset_Events=reset_form_slots(slots_names)
        return Reset_Events              