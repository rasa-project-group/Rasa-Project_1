from ..Google_connexion.Translater_connection import Translater_connection 
from rasa_sdk.executor import CollectingDispatcher
from google.cloud.translate_v2 import Client 

def Translate_message_to_other_language(current_language:str,message:str,default_language:str)->str:
    """
    Translate_message_to_other_language Translate the message depend on the selected language


    Args:
        current_language (str): current selected luanguage
        message (str): The message to be sent
        default_language(str): Defaul language
    """
    if(len(message)==0):
        return message
    if  (str.lower(current_language) != str.lower(default_language)):
        _con=Translater_connection("non for now")
        translater:Client=_con.translate_connexion 
        
        return translater.translate(message,target_language=current_language)["translatedText"]
    else:
        return message
        
            
        
    


