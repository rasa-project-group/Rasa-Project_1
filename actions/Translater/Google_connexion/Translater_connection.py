import os 

from google.cloud import translate_v2
from google.cloud.translate_v2 import Client 

class Translater_connection:
    """
    Translater_connection One connexion for google api translater
    
    Args:
    translate_connexion (translate_v2.Client)

    """
    __instance=None
     
    def __new__(cls,credential_path:str) :
        """
        __new__ inssure that their are one instalce of this class
        Args:
            credential_path (str): the path of the credential file

        Returns:
            [instance]: One instance
        """
        if cls.__instance is None:
            os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="actions/Translater/Google_connexion/rasa-translater-b690e658eeb8.json"

            cls.__instance = object.__new__(cls)
            cls.__instance.credential_path=credential_path  
            cls.__instance.translate_connexion=translate_v2.Client()
        return cls.__instance
    def __call__(cls, *args, **kwds) :
        print(f"the path is {cls.__instance.credential_path}")    

      
