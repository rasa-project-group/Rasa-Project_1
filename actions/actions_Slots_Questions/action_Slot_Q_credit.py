from actions.DAO_Chatbot.Entity.Finance_entity import Finance_statistic_entity
from rasa_sdk.types import DomainDict
from actions.DAO_Chatbot.Entity.Simulations_entities import Credit_simulation, Deposit_simulation, simulation
from typing import Any, Optional, Text, Dict, List
import pandas as pd, datetime
from rasa_sdk import Action, Tracker
from actions.DAO_Chatbot.Entity.Chatbot_info_entity import chatbot_info_entity
from rasa_sdk.events import ActionExecuted, ConversationPaused, FollowupAction, SlotSet, EventType, SessionStarted, ReminderScheduled, BotUttered, UserUtteranceReverted,ActiveLoop
from actions.DAO_Chatbot.Implementation.ImpChatbot_offer import Implchatbot_offer
from rasa_sdk.executor import CollectingDispatcher
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions.return_exception import Empty_list_exception, Note_found_exception
from rasa_sdk.interfaces import ActionExecutionRejection
from actions.Helpers.Convert_helpers import Convert_string_datetime, convert_class_offers_to_json, convert_type_to_button, \
    convert_types_to_buttons, Convert_VariableName_to_String, convert_item_id_to_buttons,convert_data_to_rocketchatAttachment
from actions.Helpers.Extract_helpers import Extract_Simulation_types_list, Extract_offers_types_list, Extract_id_string
from actions.DAO_Chatbot.Implementation import ImpChatbot_info
from actions.DAO_Chatbot.Implementation.ImpSimulation_file import Imp_Simulation_credit, Impl_Simulations_Deposit
from actions.Helpers.Credits_calcul import calcul_credit_monthly, calcul_credit_yearly
from rasa_sdk.forms import FormAction, FormValidationAction, REQUESTED_SLOT
from actions.Helpers.Validators_helpers import Is_Valid_SameCurrency, Is_Validate_intervale, Is_valide_RocketChat_Channel, Is_valid_attempt
from actions.Cache.simulations_cache import Get_Current_selected_simulation_name_list, Get_calcul_Deposit_simulation, Get_selected_simulation, \
    Get_Calcul_Credit_simulation, Get_simulations_subtypes, cancel_asking_onSlot_element, cancel_simulation, reset_form_slots
from actions.DAO_Chatbot.Implementation.ImpAgency_location import Imp_Agency_location   
from actions.GoogleMap.entitiesMap import client_cordinates  
from actions.GoogleMap.Methodes import  Get_nearest_location
from rasa.core.channels.rocketchat import     RocketChatInput
from actions.Translater.Translate_message.Translate_utter_message import Translate_message_to_other_language
import datetime
from actions.DAO_Chatbot.Implementation.ImpFinance_currency_Grpc import Imp_finance_Curreny
import random

#region Class Action ask mounted money
class Action_ask_mounted_money(Action):

    def name(self) -> Text:
        return 'action_ask_mountedmoney'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        current_selected_slote=tracker.get_slot("requested_slot")
        print(current_selected_slote)
        if(current_selected_slote=="mountedmoney"): #verify if currrent requested slote equal to deposit money
            return  []
                    
        slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
        selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
        credit_simulation: Credit_simulation = Get_selected_simulation(tracker, selected_simulation_id,
                                                                       slot_simulation_name, "Credit")

        dispatcher.utter_message(
            text=f"The credit ammount  for the  {credit_simulation.simulation_name}  must be between: {credit_simulation.Simulation_min_mounted} and {credit_simulation.Simulation_max_mounted} ")
        return []

#endregion

#region Class Action Ask period
class Action_ask_period(Action):

    def name(self) -> Text:
        return 'action_ask_period'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        current_selected_slote=tracker.get_slot("requested_slot")
        if(current_selected_slote=="period"):
            return []
        slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
        selectect_simulation_id = tracker.get_slot('selectedsimulationdetails')
        credit_simulation: Credit_simulation = Get_selected_simulation(tracker=tracker,
                                                                       selected_simulation_id=selectect_simulation_id,
                                                                       list_simulation_slot_name=slot_simulation_name)
        dispatcher.utter_message(
            text=f" The period credit  payment for the {credit_simulation.simulation_name} credit must be between: {credit_simulation.periode_min} and {credit_simulation.periode_max} ")
        return []

#endregion
