from rasa_sdk.executor import CollectingDispatcher
class Validator_Intervale_Exception(Exception):

    def __init__(self, msg: str):
        super().__init__(msg)
        self.error_message = msg
        print(msg)

    def send_message(self,dispatcher:CollectingDispatcher):
        print(self.error_message)
        dispatcher.utter_message(text=(self.error_message))
        
class Validator_SameCurrency_Exception(Exception):
    def __init__(self, msg: str):
        super().__init__(msg)
        self.error_message = msg
        print(msg)

    def send_message(self,dispatcher:CollectingDispatcher):
        print(self.error_message)
        dispatcher.utter_message(text=(self.error_message))
                    