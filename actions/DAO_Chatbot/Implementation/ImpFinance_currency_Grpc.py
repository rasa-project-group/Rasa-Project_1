from typing import List
import grpc

from actions.Grpc_Client import  finance_pb2,finance_pb2_grpc
from actions.DAO_Chatbot.Entity.Finance_entity import Finance_currency_entity,Finance_statistic_entity

class Imp_finance_Curreny(finance_pb2_grpc.Currency_Exchange_ServiceServicer):

    channel =grpc.insecure_channel("localhost:9999")
    
    stub=finance_pb2_grpc.Currency_Exchange_ServiceStub(channel)
    
    
    def Convert_currency(self,_from_currency:str,_to_currency:str,quantity:int=1)->Finance_currency_entity:
        Req_conv=finance_pb2.CurrencyConv_Request(From_currency=_from_currency,To_currenct=_to_currency,Quantity_Currency=quantity)
        Res_conv:Finance_currency_entity= self.stub.Convert_Currency(Req_conv)  
        print(Res_conv.From_Currency_Name)
        return Res_conv
    
    def Get_Currency_details_Image(self,_from_currency:str,_to_currency:str,From_date:int,To_date:int,Column_names:List,Data_type:str):
        Curency_data={"From_currency": _from_currency,"To_currenct":_to_currency,"Quantity_Currency": 10}
        Req_statistic=finance_pb2.CurrencyExchange_Image_Request(Curency=Curency_data,From_date=From_date,To_date=To_date,Column_name=Column_names,Data_type=Data_type)
        Res_stat:Finance_statistic_entity=self.stub.Get_Currency_details_Image(Req_statistic)
        return Res_stat
    
    def Get_Currency_detail_image_interval(self,_from_currency:str,_to_currency:str,From_date:int,To_date:int,Column_names:List,Data_type:str,interval_date:str="15min"):
        
        Curency_data={"Curency": {"From_currency": _from_currency,"To_currenct": _to_currency,"Quantity_Currency": 10},"From_date": 20,"To_date": 20,"Column_name":Column_names,"Data_type":Data_type}
        Req_statistic_interval=finance_pb2.CurrencyExchange_Image_Request_Interval(Basic_request=Curency_data,interval_date=interval_date)
        Res_stat=self.stub.Get_currency_details_Image_interval(Req_statistic_interval)
        return Res_stat
