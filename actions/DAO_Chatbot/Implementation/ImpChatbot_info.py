from ..Interfaces import IChatbot_info
from ..Entity import  Chatbot_info_entity
from ..Connection import Chatbot_info_con
from datetime import  datetime
import os
class Imp_Chatbot_info(IChatbot_info.IChatBot_info):


    _connection_chatbot_info=Chatbot_info_con.Chabot_info_con("actions/DB_Chatbot/Db_chatbot_info.csv")

    _path="../DB_Chatbot/Db_chatbot_info.csv"  
    
    ##
    def __init__(self,path):
        pass

    def Get_Chatbot_info(self)->Chatbot_info_entity.chatbot_info_entity:
          
      Chatbot_entity=Chatbot_info_entity.chatbot_info_entity()
      Chatbot_entity.name= self._connection_chatbot_info.Chatbot_info_data["Name"][0]+""
      Chatbot_entity.lastname=self._connection_chatbot_info.Chatbot_info_data["Lastname"][0]   
      Chatbot_entity.Age=self._connection_chatbot_info.Chatbot_info_data["Age"][0]
      Chatbot_entity.Gender=self._connection_chatbot_info.Chatbot_info_data["Gender"][0]
      Chatbot_entity.introduce=self._connection_chatbot_info.Chatbot_info_data["Introduce"][0]
      return  Chatbot_entity
