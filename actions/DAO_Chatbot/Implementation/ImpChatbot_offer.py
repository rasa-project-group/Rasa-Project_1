from typing import List
from  ..Interfaces.IChatbot_info import IChatBot_info 
from ..Entity import Offer_entity
from ..Connection import Offer_file_con
from actions.Helpers.Extract_helpers import Extract_offers_types_list, Extract_id_string

class Implchatbot_offer(IChatBot_info):

    _con=Offer_file_con.Chatbot_offer_con("actions/DB_Chatbot/Db_chatbot_offer.csv")

    def   __init__(self,path):
        pass


    def Get_Chatbot_offers(self) -> List[Offer_entity.offer]:
        Pd_offer=self._con.Chatbot_offer

        Offers =[]
        offers_images_path=Pd_offer["Image"]
        offers_Descriptions=Pd_offer["Description"]
        offers_subtitles=  Pd_offer["Subtitle"]
        offers_titles=Pd_offer["Title"] 
        offers_types=Pd_offer["Type"]
        offers_ref=Pd_offer["Ref"]
        for title,image,desc,sub,type,ref in zip(offers_titles,offers_images_path,offers_Descriptions,offers_subtitles,offers_types,offers_ref):
            print(title)
            print(ref)

            useable_offer=Offer_entity.offer()
            useable_offer.offer_id=Extract_id_string()
            useable_offer.offer_description=desc
            useable_offer.offer_Image_path=image
            useable_offer.offer_title=title
            useable_offer.offer_subtitle=sub
            useable_offer.offer_type=type
            useable_offer.offerRef_path=ref
            Offers.append(useable_offer)
        return Offers