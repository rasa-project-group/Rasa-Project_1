from typing import List
from ..Interfaces import IAgency_locations
from ..Entity.Agency_location import  Agency_location
from ..Connection import Agency_locations
from datetime import  datetime
import os
class Imp_Agency_location(IAgency_locations.IAgency_location):


    _connection_chatbot_loactions=Agency_locations.Agency_locations("actions/DB_Chatbot/DB_agency_location.csv")

    _path="../DB_Chatbot/DB_agency_location.csv"  
    
    ##
    def __init__(self,path):
        pass

    def Get_Agency_locations(self)->List[Agency_location]:
        Agency_names=self._connection_chatbot_loactions.agency_locations["agency_name"]  
        Agency_address=self._connection_chatbot_loactions.agency_locations["agency_address"]  
        Agency_latitude=self._connection_chatbot_loactions.agency_locations["agency_latitude"]
        Agency_longitude=self._connection_chatbot_loactions.agency_locations["agency_longitude"]
        Agency_infos=[]
        for name,adres,lat,lng in zip(Agency_names,Agency_address,Agency_latitude,Agency_longitude):
                agency=Agency_location()
                agency.agency_address=adres
                agency.agency_latitude=lat
                agency.agency_name=name 
                agency.agency_longitude=lng
                
                Agency_infos.append(agency)
                
                
        return  Agency_infos        