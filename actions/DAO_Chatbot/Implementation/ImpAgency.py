
from ..Entity.Agency import Agency
from ..Interfaces.IAgency import  IAgency
from ..Interfaces.IOffer_agency import IOffer
from ..Interfaces.IChatbot_info import IChatBot_info
from ..Interfaces.Idepartments import IDepartement
from ..Interfaces.ISimulation import Isimulation
from ..Connection.Chatbot_info_con import Chabot_info_con
from ..Connection.Agency_file_con import Connection_Agency_file_DB
from ..Connection.simulation_file_con import Connection_Simulation_file_DB
from ..Connection.Offer_file_con import Chatbot_offer_con
from ..Connection.Agency_file_con import  Connection_Agency_file_DB
class Imp_Agency(IAgency):
    
    _Imp_offer:IOffer
    _Imp_Chabot_info:IChatBot_info
    _Imp_depaetement:IDepartement
    _Imp_simulation:Isimulation
    _con=Connection_Agency_file_DB
    def __init__(self,impl_offer:IOffer,impl_chatbot_info:IChatBot_info,impl_simulation:Isimulation,path:str):
        self._Imp_Chabot_info=impl_chatbot_info
        self._Imp_offer=impl_offer
        self._Imp_simulation=impl_simulation
        pass
    
    
    def Get_Agency(self) -> Agency:
        _agency=Agency()
        _agency.Chatbot_info=self._Imp_Chabot_info.Get_Chatbot_info()
        _agency.Agency_offers=self._Imp_offer.Get_Chatbot_offers()
        _agency.Simulations=self._Imp_simulation.Get_simulations()
        return _agency


