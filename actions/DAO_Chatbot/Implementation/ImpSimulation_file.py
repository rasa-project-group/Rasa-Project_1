from typing import Dict, Iterator, List, Text
from ..Interfaces.ISimulation import Isimulation_Credit, Isimulation_Deposit
from ..Connection.simulation_file_credit_con import Connection_Simulation_file_DB_Credit
from ..Entity.Simulations_entities import Deposit_simulation, Credit_simulation, simulation
from actions.Helpers.Extract_helpers import Extract_offers_types_list, Extract_id_string
from actions.DAO_Chatbot.Connection.simulation_file_deposit_con import Connection_Simulation_file_DB_Deposit
from pandas import DataFrame

class Imp_Simulation_credit(Isimulation_Credit):
    _con = Connection_Simulation_file_DB_Credit('actions/DB_Chatbot/Db_chatbot_simulation_credit.csv')

    def __init__(self, path=''):
        pass

    def Get_simulations_Credit(self) -> List[Credit_simulation]:
        Pd_reader = self._con.simulation_credit
        simulations_list = Get_simulation_lists(Pd_reader)
        return simulations_list


class Impl_Simulations_Deposit(Isimulation_Deposit):
    _con = Connection_Simulation_file_DB_Deposit('actions/DB_Chatbot/Db_chatbot_simulation_deposit.csv')

    def __init__(self, path=''):
        pass

    def Get_simulations_Deposit(self) -> List[Deposit_simulation]:
        Pd_reader = self._con.simulation_deposit
        simulations_deposit_list = []
        simulations_list = Get_simulation_lists(Pd_reader)
        Simulations_Initial_min = Pd_reader['initial_deposit_min']
        Simulations_Initial_max = Pd_reader['initial_deposit_max']
        for simulation, initial_max, initial_min in zip(simulations_list, Simulations_Initial_max, Simulations_Initial_min):
            _simulation_deposit = simulation
            _simulation_deposit.initial_deposit_max = initial_max
            _simulation_deposit.initial_deposit_min = initial_min
            simulations_deposit_list.append(_simulation_deposit)

        return simulations_deposit_list


def Get_simulation_lists(dataframe) -> List[simulation]:
    simulations = []
    max_mounted = dataframe['mounted_max']
    min_mounted = dataframe['mounted_min']
    Simulations_name = dataframe['simulation_name']
    Simulations_details = dataframe['simulation_details']
    Simulations_period_max = dataframe['period_max']
    Simulations_period_min = dataframe['period_min']
    Simulations_types = dataframe['simulation_type']
    added_value = dataframe['added_value']
    for name, detail, per_max, per_min, type, added_val, max_m, min_m in zip(Simulations_name, Simulations_details, Simulations_period_max, Simulations_period_min, Simulations_types, added_value, max_mounted, min_mounted):
        _simulation = simulation()
        _simulation.simulation_id = Extract_id_string()
        _simulation.simulation_name = name
        _simulation.simulation_details = detail
        _simulation.periode_max = per_max
        _simulation.periode_min = per_min
        _simulation.Simulation_type = type
        _simulation.Simulation_added_value = added_val
        _simulation.Simulation_min_mounted = min_m
        _simulation.Simulation_max_mounted = max_m
        simulations.append(_simulation)

    return simulations