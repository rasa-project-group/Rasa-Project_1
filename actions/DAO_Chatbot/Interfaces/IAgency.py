from abc import ABC, abstractmethod
from ..Entity.Agency import Agency
class IAgency:
    @abstractmethod
    def Get_Agency()->Agency:
        pass