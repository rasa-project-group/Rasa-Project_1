from abc import ABC, abstractmethod
from typing import List
from ..Entity.Agency_location import Agency_location
class IAgency_location:
    @abstractmethod
    def Get_Agency_locations()->List[Agency_location]:
        pass