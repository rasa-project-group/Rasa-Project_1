from typing import List
from ..Entity import Offer_entity
from abc import ABC, abstractmethod
class IOffer:
   @abstractmethod
   def Get_Chatbot_offers(self)-> List[Offer_entity.offer]:
       pass
