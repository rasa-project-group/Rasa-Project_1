from abc import ABC, abstractmethod
from typing import Dict, List, Text
from ..Entity.Simulations_entities import Credit_simulation, Deposit_simulation, simulation

class Isimulation_Deposit:

    @abstractmethod
    def Get_simulations_Deposit(self) -> List[Deposit_simulation]:
        pass


class Isimulation_Credit:

    @abstractmethod
    def Get_simulations_Credit(self) -> List[Credit_simulation]:
        pass