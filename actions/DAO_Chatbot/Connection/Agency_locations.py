import pandas as pd
class Agency_locations(): 
    
    __instance=None

    def __new__(cls,path) :
        if cls.__instance is None:
            cls.__instance = object.__new__(cls)
            cls.__instance.path=path  
            cls.__instance.agency_locations=pd.read_csv(path)
        return cls.__instance
    def __call__(cls, *args, **kwds) :
        print(f"the path is {cls.__instance.path}")          

  
