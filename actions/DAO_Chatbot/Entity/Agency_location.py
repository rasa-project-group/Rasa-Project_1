class Agency_location():

    def __init__(self) :
        pass   
         
    @property
    def agency_name(self)->str:
           return  self._agency_name
       
    @agency_name.setter
    def agency_name(self,name:str):
        self._agency_name=  name       

    @property 
    def agency_address(self)->str:
        return self._agency_address
    @agency_address.setter
    def agency_address(self,address):
        self._agency_address=  address
        
    @property   
    def agency_latitude(self)->str:
        return self._agency_latitude
    @agency_latitude.setter
    def agency_latitude(self,lat:str):
        self._agency_latitude=lat
        
        
    @property
    def agency_longitude(self)->str:
        return self._agency_longitude
    @agency_longitude.setter
    def agency_longitude(self,longt:str):
        self._agency_longitude=longt
