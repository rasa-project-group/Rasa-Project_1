import datetime as dt


class chatbot_info_entity():
    def __init__(self) :
            pass
    @property
    def name(self)->str:
        return self._name
    @name.setter
    def name(self,name)->str:
         self._name=name
         
         
    @property
    def lastname(self)->str:
        return self._lastname
    
    @lastname.setter
    def lastname(self,lastname):
        self._lastname=lastname
        
    @property
    def full_name(self)->str:
        return "{} {}".format(self._lastname,self._name)
    
    @property
    def Age(self)->str:
         return self._Age
    @Age.setter
    def Age (self,age):
         self._Age=age
    @property
    def Gender(self)->str:
        return self._Gender
    
    @Gender.setter
    def Gender(self,gender):
         self._Gender=gender
    @property
    def introduce(self)->str:
        return self._introduce
    @introduce.setter
    def introduce(self,introduce):
        self._introduce=introduce
    
    
      