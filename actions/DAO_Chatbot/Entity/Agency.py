from typing import List
from .Offer_entity import offer
from .Simulations_entities import Credit_simulation , Deposit_simulation

from .Chatbot_info_entity import chatbot_info_entity
class Agency():
    def __init__(self) :
        pass  
@property
def Agency_name(self)->str:
    return self.__Agency_name;
@Agency_name.setter
def Agency_name(self,name:str):
    self.Agency_name=name   
@property 
def Aagency_creation_date(self)->str:
    return self.__Aagency_creation_date

@Aagency_creation_date.setter    
def Aagency_creation_date(self,date:str):
    self.Aagency_creation_date=date

@property
def Agency_email(self)->str:
    return self.__Agency_email
@Agency_email.setter 
def Agency_email(self,Email:str):
    self.Agency_email=Email
    
@property 
def Chatbot_info(self)->chatbot_info_entity:
    return self.__Chatbot_info
@Chatbot_info.setter
def Chatbot_info(self,chatbot_inf:chatbot_info_entity):
    self.Chatbot_info=chatbot_inf
@property 
def Agency_offers(self)->list:
    self.__Agency_offers   
    
@Agency_offers.setter
def Agency_offers(self,offers:list):
    self.Agency_offers=offers
@property
def Simulations(self)->list:
    return self.__Simulations    
@Simulations.setter 
def Simulations(self,simulations:list):
    self.Simulations=simulations