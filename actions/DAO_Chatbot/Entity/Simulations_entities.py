class simulation:

    def __init__(self):
        pass


    @property
    def simulation_id(self) -> str:
        return self._simulation_id
    
    
    @simulation_id.setter
    def simulation_id(self, sum_id:str) -> str:
        self._simulation_id = sum_id
    
    
    @property
    def simulation_name(self) -> str:
        return self._simulation_name
    
    
    @simulation_name.setter
    def simulation_name(self, name):
        self._simulation_name = name
    
    
    @property
    def simulation_details(self) -> str:
        return self._simulation_details
    
    
    @simulation_details.setter
    def simulation_details(self, details: str):
        self._simulation_details = details
    
    
    @property
    def periode_max(self) -> int:
        return self._periode_max
    
    
    @periode_max.setter
    def periode_max(self, p_max: int):
        self._periode_max = p_max
    
    
    @property
    def periode_min(self) -> int:
        return self._periode_min
    
    
    @periode_min.setter
    def periode_min(self, p_min: int):
        self._periode_min = p_min
    
    
    @property
    def Simulation_type(self) -> str:
        return self._Simulation_type
    
    
    @Simulation_type.setter
    def Simulation_type(self, Sim_type: str):
        self._Simulation_type = Sim_type
    
    
    @property
    def Simulation_added_value(self) -> float:
        return self._Simulation_added_value
    
    
    @Simulation_added_value.setter
    def Simulation_added_value(self, value: float):
        self._Simulation_added_value = value
    
    
    @property
    def Simulation_min_mounted(self) -> float:
        return self._Simulation_min_mounted
    
    
    @Simulation_min_mounted.setter
    def Simulation_min_mounted(self, min_mounted: float):
        self._Simulation_min_mounted = min_mounted
    
    
    @property
    def Simulation_max_mounted(self) -> float:
        return self._Simulation_max_mounted
    
    
    @Simulation_max_mounted.setter
    def Simulation_max_mounted(self, max_mounted: float):
        self._Simulation_max_mounted = max_mounted
    
    
      
    def __str__(self):
            return {'simulation_name':self.simulation_name,  'simulation_details':self.simulation_details,  'periode_max':self.periode_max, 
             'periode_min':self.periode_min,  'Simulation_type':self.Simulation_type, 
             'Simulation_added_value':self.Simulation_added_value, 
             'simulation_id':self.simulation_id, 
             'Simulation_max_mounted':self.Simulation_max_mounted,  'Simulation_min_mounted':self.Simulation_min_mounted}


class Deposit_simulation(simulation):

    def __init__(self):
        pass


    @property
    def initial_deposit_max(self) -> int:
        return self._initial_deposit_max
    
    
    @initial_deposit_max.setter
    def initial_deposit_max(self, init_max: int):
        self._initial_deposit_max = init_max
    
    
    @property
    def initial_deposit_min(self) -> int:
        return self._initial_deposit_min
    
    
    @initial_deposit_min.setter
    def initial_deposit_min(self, init_min: int):
        self._initial_deposit_min = init_min
    @property
    def mounted_money(self) -> float:
        return self._mounted_money
    
    
    @mounted_money.setter
    def mounted_money(self, mounted: float):
        self._mounted_money = mounted        
    @property
    def initial_payment(self) -> float:
        return self._initial_payment
    
    
    @initial_payment.setter
    def initial_payment(self, initial_payment: float):
        self._initial_payment = initial_payment            
    @property
    def period(self) -> int:
        return self._period
    
    
    @period.setter
    def mounted_money(self, period: int):
        self._period = period
class Credit_simulation(simulation):

    def __init__(self):
        pass


    @property
    def mounted_money(self) -> float:
        return self._mounted_money
    
    
    @mounted_money.setter
    def mounted_money(self, mounted: float):
        self._mounted_money = mounted
    @property
    def period(self) -> int:
        return self._period
    
    
    @period.setter
    def mounted_money(self, period: int):
        self._period = period