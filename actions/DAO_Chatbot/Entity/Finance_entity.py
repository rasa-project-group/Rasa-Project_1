

from typing import Dict, Text


class Finance_currency_entity():
    def __init__(self) :
            pass
    @property
    def From_Currency_Name(self)->str:
        return self._From_Currency_Name
    @From_Currency_Name.setter
    def From_Currency_Name(self,name)->str:
         self._From_Currency_Name=name
         
         
    @property
    def To_Currency_Name(self)->str:
        return self._To_Currency_Name
    
    @To_Currency_Name.setter
    def To_Currency_Name(self,name):
        self._To_Currency_Name=name
        

    @property
    def Exchange_Rate(self)->float:
         return  self._Exchange_Rate
    @Exchange_Rate.setter
    def Exchange_Rate (self,rate):
         self._Exchange_Rate=rate
    @property
    def Bid_price(self)->float:
        return self._Bid_price
    
    @Bid_price.setter
    def Bid_price(self,price):
         self._Bid_price=price
    @property
    def Ask_price(self)->float:
        return self._Ask_price
    @Ask_price.setter
    def Ask_price(self,price):
        self._Ask_price=price
    @property
    def Last_Refreshed(self)->str:
        return self._Last_Refreshed
    @Last_Refreshed.setter
    def Last_Refreshed(self,date)->str:
         self._Last_Refreshed=date
class Finance_statistic_entity():
    @property
    def Image_base64(self)->Text:
        return self._Image_base64
    @Image_base64.setter
    def Image_base64(self,src)->Text:
         self._Image_base64=src
    @property
    def image_detail(self)->Dict[Text,Text]:
        return self._image_detail
    @image_detail.setter
    def image_detail(self,derails)->Dict[Text,Text]:
         self._image_detail=derails                                 