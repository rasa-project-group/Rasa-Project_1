
class offer():
    """
    offer [summary]

    [extended_summary]
    """
    def __init__(self) :
        pass   
    @property
    def offer_id(self)->str:
           return  self._offer_id
    @offer_id.setter
    def offer_id(self,uid:str)->str:
            self._offer_id=uid          
    @property
    def offer_description(self)->str:
           return  self._offer_description
    @offer_description.setter 
    def offer_description(self,Desc:str):
        self._offer_description=Desc   
    @property 
    def offer_type(self)->str:
        return self._offer_type
    @offer_type.setter
    def offer_type(self,offer_t):
        self._offer_type=  offer_t
    @property   
    def offer_title(self)->str:
        return self._offer_title
    @offer_title.setter
    def offer_title(self,title:str):
        self._offer_title=title
    @property
    def offer_subtitle(self)->str:
        return self._offer_subtitle
    @offer_subtitle.setter
    def offer_subtitle(self,of_sub:str):
        self._offer_subtitle=of_sub
    @property
    def offer_Image_path(self)->str:
        return self._offer_Image_path 
    @offer_Image_path.setter
    def offer_Image_path(self,path:str):
        self._offer_Image_path=path

    @property
    def offerRef_path(self)->str:
        return self._offerRef_path
    @offerRef_path.setter
    def offerRef_path(self,path:str):
        self._offerRef_path=path
   