from typing import List
from .entitiesMap  import client_cordinates
from ..Helpers.Extract_helpers import Extract_DestinationAndOrigin

from .Client import Client_map
from .lib.Map_requests import distance_matrix

def Get_nearest_location(location:client_cordinates,Distinations:List):
    Client=Client_map()
    print(f"lat{location.lat} , lng{location.lng}")
    Res=distance_matrix(client=Client.CLIENT_MAP,origins=[{"lat":location.lat,"lng":location.lng}],destinations=Distinations)
    print(Res)
    return Extract_DestinationAndOrigin(Res)
    
    