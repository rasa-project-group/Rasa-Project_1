from typing import Dict, Text


class Matrix_element:
    @property
    def Dest(self)->str:
           return  self._Dest
       
    @Dest.setter
    def Dest(self,dest:str):
        self._Dest=  dest       

    @property 
    def Origin(self)->str:
        return self._Origin
    @Origin.setter
    def Origin(self,org:str):
        self._Origin=  org
        
    @property   
    def distance(self)->Dict[Text,Text]:
        return self._distance
    @distance.setter
    def distance(self,dist:Dict[Text,Text]):
        self._distance=dist
        
        
    @property
    def Duration(self)->Dict:
        return self._Duration
    @Duration.setter
    def Duration(self,dur:Dict):
        self._Duration=dur
    @property 
    def lat(self)->str:
        return self._lat
    @lat.setter
    def lat(self,lat:str):
        self._lat=  lat 
        
    @property 
    def lng(self)->str:
        return self._lng
    @lng.setter
    def lng(self,lng:str):
        self._lng=  lng        
        
class client_cordinates:
    @property 
    def lat(self)->str:
        return self._lat
    @lat.setter
    def lat(self,lat:str):
        self._lat=  lat 
        
    @property 
    def lng(self)->str:
        return self._lng
    @lng.setter
    def lng(self,lng:str):
        self._lng=  lng
    @property 
    def precision(self)->str:
        return self._precision
    @precision.setter
    def lng(self,prec:str):
        self._precision=  prec
        
                                      