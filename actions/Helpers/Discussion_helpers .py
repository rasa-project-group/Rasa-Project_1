from rasa_sdk.executor import CollectingDispatcher,Tracker


def repetited_OfDiscussion(dispatcher:CollectingDispatcher,tracker:Tracker,repeted_messages=[],action_name=""):
    repeted:bool=False
    if len(repeted_messages)==0:
        return  
    if(action_name==""):
        return 
    for index, msg in enumerate(repeted_messages):
        
        if(tracker.last_executed_action_has(action_name,index)):
            dispatcher.utter_message(text=msg) 
            repeted=True
            break
    return repeted  
   