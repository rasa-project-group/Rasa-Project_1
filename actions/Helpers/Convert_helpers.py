from typing import List
from ..DAO_Chatbot.Entity.Simulations_entities import simulation
from varname import nameof
from ..DAO_Chatbot.Entity.Offer_entity import offer
import datetime
def Convert_VariableName_to_String(varialble) -> str:
    if varialble is None:
        return ''
    else:
        return nameof(varialble)


def convert_class_offers_to_json(_offer: offer):
    return {'offer_title':_offer.offer_title, 
     'offer_description':_offer.offer_description,  'offer_Image_path':_offer.offer_Image_path,  'offer_subtitle':_offer.offer_subtitle,  'offer_type':_offer.offer_type}


def convert_types_to_buttons(title_list: list, slot_name: str, intent_name: str) -> list:
    """
    convert_types_to_buttons Convert list of string  to   json buttons format


    Args:
        title_list (list): list of button title
        slot_name (str): slot that will take the selected value
        intent_name (str): Traget intent that will fire the story chaine

    Returns:
        list: list of json 
    """
    buttons = []
    for Text_type in title_list:
        Button_json = convert_type_to_button(Text_type, slot_name, Text_type, intent_name)
        buttons.append(Button_json)

    return buttons


def convert_type_to_button(button_title: str, slot_name: str, val_slot: str, intent_name: str) -> dict:
    """
    convert_type_to_button convert one type to   json  button format


    Args:
        button_title (str): button title
        slot_name (str): target slot name
        val_slot (str): slot value
        intent_name (str): Traget intent that will fire the story chaine

    Returns:
        dict: json for buttons 
    """
    title_text = '/' + intent_name + '{"' + slot_name + '":' + '"' + val_slot + '"}'
    return {'title':button_title,  'payload':title_text}


def convert_item_id_to_buttons(name_id_list: list, slot_name: str, intent_name: str) -> dict:
    buttonss = []
    for item in name_id_list:
        name = item['name']
        id = item['id']
        title_text = '/' + intent_name + '{"' + slot_name + '":' + '"' + id + '"}'
        print(title_text)
        buttonss.append({'title':name,  'payload':title_text})

    return buttonss

def convert_list_of_variables_to_list(class_varibles:dict):
    variables_names_list:List[str]=[]
    
def convert_class_simulation_to_json(_simulation: simulation) -> dict:
    return {'simulation_name':_simulation.simulation_name,  'simulation_details':_simulation.simulation_details,  'period_max':_simulation.periode_max,  'periode_min':_simulation.periode_min,  'Simulation_type':_simulation.Simulation_type,  'Simulation_added_value':_simulation.Simulation_added_value}
def Convert_string_datetime(date_str:str,format:str):
    date=datetime.datetime.strptime(date_str,'%Y-%m-%d %H:%M:%S')
    return date.strftime(format)
  
def convert_data_to_rocketchatAttachment(Attachment_type:str,attachment_data:List)->dict:

    return {"attachments_type":Attachment_type,"attachments_data":attachment_data}
    