from rasa_sdk.events import ActiveLoop, SlotSet
from actions.Cache.simulations_cache import cancel_asking_onSlot_element
from enum import Enum
from typing import Text

from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.interfaces import Tracker

class Channels_types (Enum):
    ROCKETCHAT="ROCKETCHAT"
    Twilio="ROCKETCHAT"
    
def Is_Validate_intervale(_min_mounted_money: float, _max_mounted_money: float, _mounted_money: float) -> bool:
    print(_min_mounted_money)
    print(_max_mounted_money)
    print(_min_mounted_money)
    if _mounted_money < _min_mounted_money or _mounted_money > _max_mounted_money:
        return False
    elif _mounted_money >= _min_mounted_money:
        if _mounted_money <= _max_mounted_money:
            pass
        return True
    else:
        return False
    

def Is_valide_RocketChat_Channel(Channel_name:str)->bool:
    """
    Is_valide_RocketChat_Channel Validate RocketChat channel name
    Args:
        Channel_name ([type]): [description]
    """
    print(Channels_types.ROCKETCHAT.name)
    print(str.upper(Channel_name))
    if(str.upper(Channel_name)==Channels_types.ROCKETCHAT.name+""):
        return True
    else:
        return False
def Is_valid_attempt(dispatcher:CollectingDispatcher,text:Text,Max_attempts:int,currenct_attempts:int,tracker:Tracker):
    if(currenct_attempts< Max_attempts and currenct_attempts>0):         
        dispatcher.utter_message(text=text)
        return[]
    elif (currenct_attempts>=Max_attempts):
        return {"requested_slot":None}    
    else:
        return []    
def Is_Valid_SameCurrency(fromCurency:str,toCurrenct:str):
    if( fromCurency==None or   toCurrenct==None):
        return False
    elif(str.upper(fromCurency)==str.upper(toCurrenct)) :
        return False
    else:
        return True
       