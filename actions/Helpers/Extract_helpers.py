from typing import Dict, List, Set, Text
from ..DAO_Chatbot.Entity.Offer_entity import offer
import uuid
from actions.GoogleMap.entitiesMap import Matrix_element 
def Extract_offers_types_list(offers_list: list)->Set[Text]:
    offers_types:Set[Text] = set()
    for offer in offers_list:
        offers_types.add(offer['offer_type'])

    return offers_types


def Extract_Simulation_types_list(simulations_list: list, simulation_type: str="") -> Set[Text]: 
    
    simulations_types = set()
    [simulations_types.add(Simulation['Simulation_type']) for Simulation in simulations_list]
    return simulations_types


def Extract_id_string() -> str:
    return str(uuid.uuid4())


def Extract_DestinationAndOrigin(Res:Dict[Text,List])->Dict[Text,List]:

    if not Res:
        return ;
    
    Dest_Address=Res["destination_addresses"]
    Origins_adress=Res["origin_addresses"]
    Rows_elements=Res["rows"]
    if not  Dest_Address or  not  Origins_adress:
        return 
    Destination_size=len(Dest_Address)
    origins_size=len(Origins_adress)
    if(Destination_size==0 or origins_size==0):
        return
    Elements=[]
    origin_adress=Origins_adress[0]
    for Dest , row_element in zip( Dest_Address,Rows_elements[0]["elements"]):
        elemen=Matrix_element()
        elemen.Origin=origin_adress
        elemen.distance=row_element["distance"]
        elemen.Duration=row_element["duration"]
        elemen.Dest=Dest
        
        Elements.append({"Origin":elemen.Origin,"Dest":elemen.Dest,"Duration":elemen.Duration,"distance":elemen.distance})
        
  
    sorteds=sorted(Elements,key=lambda v:(v["distance"]["text"]),reverse=False)

    return    sorteds 