import unittest
from unittest import mock
from unittest.mock import Mock, patch

from rasa_sdk.interfaces import Tracker
import pytest
from actions.Cache.simulations_cache import  Get_selected_simulation
from actions.DAO_Chatbot.Exceptions.return_exception import Note_found_exception,Empty_list_exception


def fake_get_slot( key):
        fake_data={"simulation_credit_list":[{"simulation_id":"dsdsd112211212"},{"simulation_id":"azeazeaze"},{"simulation_id":"dqsdqsd"}]}
        if key in fake_data:return fake_data[key]  
        else:return None
        
@pytest.mark.parametrize(["simulation_id","slot_selected_name"],[("dsdsd112211212","simulation_dsdqsd_list")])
@mock.patch("rasa_sdk.interfaces.Tracker")
def test_Get_SelectedsimulationAndSimulationSlotName_ReturnNone( mock_tracker,simulation_id:str,slot_selected_name:str):
        ##Arange
        mock_tracker.get_slot.side_effect=fake_get_slot
        ##Action  && Assert
        with pytest.raises( Note_found_exception,match="I think the kind of simualtion that you looking for dose not exist any more") as error:
             Get_selected_simulation(mock_tracker,simulation_id,slot_selected_name,"")
                
        
        
@pytest.mark.parametrize(["selected_simulation_id","slot_selected_name"],[("xxxcdsq","simulation_credit_list")])
@mock.patch("rasa_sdk.interfaces.Tracker")
def test_Get_SelectedsimulationAndSimulationSlotName_RetunrEmptyList( mock_tracker,selected_simulation_id:str,slot_selected_name:str):
        ##Arange
        
        mock_tracker.get_slot.side_effect=fake_get_slot
        
        ##Action && Assert
        with pytest.raises( Empty_list_exception,match=f"There is no simulation with id equal to{selected_simulation_id}") as error:
             Get_selected_simulation(mock_tracker,selected_simulation_id,slot_selected_name,"")
             
@pytest.mark.parametrize(["selected_simulation_id","slot_selected_name"],[("dqsdqsd","simulation_credit_list")])
@mock.patch("rasa_sdk.interfaces.Tracker")
def test_Get_SelectedsimulationAndSimulationSlotName_InvalidJson( mock_tracker,selected_simulation_id:str,slot_selected_name:str):
        ##Arange
        
        mock_tracker.get_slot.side_effect=fake_get_slot
        
        ##Action && Assert
        with pytest.raises( KeyError,match=f"There is no simulation with id equal to{selected_simulation_id}") as error:
             Get_selected_simulation(mock_tracker,selected_simulation_id,slot_selected_name,"")                
        
        

          
