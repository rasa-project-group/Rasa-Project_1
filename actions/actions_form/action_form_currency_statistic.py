
from actions.DAO_Chatbot.Entity.Finance_entity import Finance_statistic_entity
from rasa_sdk.types import DomainDict
from actions.DAO_Chatbot.Entity.Simulations_entities import Credit_simulation, Deposit_simulation, simulation
from typing import Any, Optional, Text, Dict, List
import pandas as pd, datetime
from rasa_sdk import Action, Tracker
from actions.DAO_Chatbot.Entity.Chatbot_info_entity import chatbot_info_entity
from rasa_sdk.events import ActionExecuted, ConversationPaused, FollowupAction, SlotSet, EventType, SessionStarted, ReminderScheduled, BotUttered, UserUtteranceReverted,ActiveLoop
from actions.DAO_Chatbot.Implementation.ImpChatbot_offer import Implchatbot_offer
from rasa_sdk.executor import CollectingDispatcher
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions.return_exception import Empty_list_exception, Note_found_exception
from rasa_sdk.interfaces import ActionExecutionRejection
from actions.Helpers.Convert_helpers import Convert_string_datetime, convert_class_offers_to_json, convert_type_to_button, \
    convert_types_to_buttons, Convert_VariableName_to_String, convert_item_id_to_buttons,convert_data_to_rocketchatAttachment
from actions.Helpers.Extract_helpers import Extract_Simulation_types_list, Extract_offers_types_list, Extract_id_string
from actions.DAO_Chatbot.Implementation import ImpChatbot_info
from actions.DAO_Chatbot.Implementation.ImpSimulation_file import Imp_Simulation_credit, Impl_Simulations_Deposit
from actions.Helpers.Credits_calcul import calcul_credit_monthly, calcul_credit_yearly
from rasa_sdk.forms import FormAction, FormValidationAction, REQUESTED_SLOT
from actions.Helpers.Validators_helpers import Is_Valid_SameCurrency, Is_Validate_intervale, Is_valide_RocketChat_Channel, Is_valid_attempt
from actions.Cache.simulations_cache import Get_Current_selected_simulation_name_list, Get_calcul_Deposit_simulation, Get_selected_simulation, \
    Get_Calcul_Credit_simulation, Get_simulations_subtypes, cancel_asking_onSlot_element, cancel_simulation, reset_form_slots
from actions.DAO_Chatbot.Implementation.ImpAgency_location import Imp_Agency_location   
from actions.GoogleMap.entitiesMap import client_cordinates  
from actions.GoogleMap.Methodes import  Get_nearest_location
from rasa.core.channels.rocketchat import     RocketChatInput
from actions.Translater.Translate_message.Translate_utter_message import Translate_message_to_other_language
import datetime
from actions.DAO_Chatbot.Implementation.ImpFinance_currency_Grpc import Imp_finance_Curreny
import random

class Action_validate_currency_statistic(FormValidationAction):
    additional_slots = []
    Fixed_columns=["close","high","open","low"]
    

    def name(self) -> Text:
        return "validate_currencyExchangestatistic_form"
    async def required_slots(self, slots_mapped_in_domain: List[Text], dispatcher: "CollectingDispatcher", tracker: "Tracker", domain: "DomainDict") -> List[Text]:
        if tracker.slots.get("DateCurrencyType") == "interval":
            self.additional_slots.append("intervalduration")
            print(slots_mapped_in_domain)
        return  slots_mapped_in_domain+ self.additional_slots 
    async def extract_intervalduration(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        text_of_last_user_message = tracker.latest_message.get("text")
        if(text_of_last_user_message not in ["15min","1min","30min","60min"]):
            
            return {"intervalduration": None}
        else:
            return {"intervalduration": text_of_last_user_message}

    async def validate_intervalduration(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
                               domain: DomainDict) -> Dict[(Text, Any)]:
        return {"intervalduration":slot_value}
    
    async def validate_fromcurrency(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
                               domain: DomainDict) -> Dict[(Text, Any)]:
        return {"fromcurrency":slot_value}
    
    async def validate_tocurrency(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
                               domain: DomainDict) -> Dict[(Text, Any)]:
        from_currency=tracker.get_slot("fromcurrency")
        if(Is_Valid_SameCurrency(slot_value,from_currency)):
            
            return {"tocurrency":slot_value}
        else:return {"tocurrency":None}
        
    async def validate_elementlist(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
                               domain: DomainDict) -> Dict[(Text, Any)]:
        if cancel_asking_onSlot_element(dispatcher,tracker,"stop_filling_list") :
            
            return {'elementlist':slot_value} 
        else:
            selected_column_list:List=tracker.get_slot("ColumnsCurrencyExchange") or []
            if(selected_column_list==None):
                selected_column_list=[]
            if(len(selected_column_list)==3):
                selected_column_list.append(slot_value)
                
                return {'elementlist':slot_value,"ColumnsCurrencyExchange":selected_column_list}
            elif(len(selected_column_list)<4):     
                
                if(slot_value  in selected_column_list ):
                    dispatcher.utter_message(text=f"you have already chosed this {slot_value} ")
                    return {'elementlist':None}
                else:
                    selected_column_list.append(slot_value)
                    return {'elementlist':None,"ColumnsCurrencyExchange":selected_column_list}

    async def  validate_DateCurrencyType(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
                               domain: DomainDict) -> Dict[(Text, Any)]:
        
        return {"DateCurrencyType":slot_value}
class Action_Call_statistic(Action):
    def name(self) -> Text:
        return "action_convert_statistic_currency"
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        statistic_imp=Imp_finance_Curreny()
        plots:List=tracker.get_slot("ColumnsCurrencyExchange")
        from_currency=tracker.get_slot("fromcurrency")
        to_currency=tracker.get_slot("tocurrency")
        Data_type=tracker.get_slot("DateCurrencyType")
        Interval:str=tracker.get_slot("intervalduration")
        if(Interval==None):
            Statistic_res:Finance_statistic_entity=statistic_imp.Get_Currency_details_Image(from_currency,to_currency,20,20,plots,Data_type=Data_type)
        else:
            Statistic_res:Finance_statistic_entity=statistic_imp.Get_Currency_detail_image_interval(from_currency,to_currency,20,20,plots,Data_type=Data_type,interval_date=tracker.get_slot("intervalduration"))
            print(Statistic_res.Image_base64)
            
            
        
        dispatcher.utter_message(text=Statistic_res.image_detail.Image_title,image=Statistic_res.Image_base64)
        return [FollowupAction("action_reset_form_currency_statistic")]      