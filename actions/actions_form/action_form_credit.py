from actions.DAO_Chatbot.Entity.Finance_entity import Finance_statistic_entity
from rasa_sdk.types import DomainDict
from actions.DAO_Chatbot.Entity.Simulations_entities import Credit_simulation, Deposit_simulation, simulation
from typing import Any, Optional, Text, Dict, List
import pandas as pd, datetime
from rasa_sdk import Action, Tracker
from actions.DAO_Chatbot.Entity.Chatbot_info_entity import chatbot_info_entity
from rasa_sdk.events import ActionExecuted, ConversationPaused, FollowupAction, SlotSet, EventType, SessionStarted, ReminderScheduled, BotUttered, UserUtteranceReverted,ActiveLoop, UserUttered
from actions.DAO_Chatbot.Implementation.ImpChatbot_offer import Implchatbot_offer
from rasa_sdk.executor import CollectingDispatcher
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions.return_exception import Empty_list_exception, Note_found_exception
from rasa_sdk.interfaces import ActionExecutionRejection
from actions.Helpers.Convert_helpers import Convert_string_datetime, convert_class_offers_to_json, convert_type_to_button, \
    convert_types_to_buttons, Convert_VariableName_to_String, convert_item_id_to_buttons,convert_data_to_rocketchatAttachment
from actions.Helpers.Extract_helpers import Extract_Simulation_types_list, Extract_offers_types_list, Extract_id_string
from actions.DAO_Chatbot.Implementation import ImpChatbot_info
from actions.DAO_Chatbot.Implementation.ImpSimulation_file import Imp_Simulation_credit, Impl_Simulations_Deposit
from actions.Helpers.Credits_calcul import calcul_credit_monthly, calcul_credit_yearly
from rasa_sdk.forms import FormAction, FormValidationAction, REQUESTED_SLOT
from actions.Helpers.Validators_helpers import Is_Valid_SameCurrency, Is_Validate_intervale, Is_valide_RocketChat_Channel, Is_valid_attempt
from actions.Cache.simulations_cache import Get_Current_selected_simulation_name_list, Get_calcul_Deposit_simulation, Get_selected_simulation, \
    Get_Calcul_Credit_simulation, Get_simulations_subtypes, cancel_asking_onSlot_element, cancel_simulation, reset_form_slots
from actions.DAO_Chatbot.Implementation.ImpAgency_location import Imp_Agency_location   
from actions.GoogleMap.entitiesMap import client_cordinates  
from actions.GoogleMap.Methodes import  Get_nearest_location
from rasa.core.channels.rocketchat import     RocketChatInput
from actions.Translater.Translate_message.Translate_utter_message import Translate_message_to_other_language
import datetime
from actions.DAO_Chatbot.Implementation.ImpFinance_currency_Grpc import Imp_finance_Curreny
import random

class Action_Form_credit_validation(FormValidationAction):
    selected_simulation_id: str = None
    Credit_simulation_entity:Credit_simulation
    Counter=0
    
    Max_attempts=3;
    Attempts=0;
    async def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: DomainDict) -> List[EventType]:
        Events_listners=[]
        Events_listners=cancel_simulation(dispatcher,tracker)
        
        if(len(Events_listners)==0):
            print(Events_listners)
            return  await super().run(dispatcher, tracker, domain)
        else: 
            if(self.Counter==0):
                dispatcher.utter_message(text=f"You have canceled the  simulation process    and you can restart the operation if you wish.")
                print(self.Counter)
                self.Counter=self.Counter+1


            return [SlotSet("requested_slot",None)]       
           
    def name(self) -> Text:
        return 'validate_calculcredit_form'
    async def validate_mountedmoney(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
                              domain: DomainDict) -> Dict[(Text, Any)]:
        """Validate mountedmoney value."""
        
        print(tracker.get_intent_of_latest_message())
        print(tracker.latest_message)
        slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
        self.selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
        print(slot_simulation_name)
        self.Credit_simulation_entity = Get_selected_simulation(tracker, self.selected_simulation_id, slot_simulation_name)
        sol1=Get_selected_simulation(tracker, self.selected_simulation_id, slot_simulation_name)
        print(f'max_possible_mounted:{sol1.Simulation_max_mounted},min:{sol1.Simulation_min_mounted}')
        
        min_possible_mounted = self.Credit_simulation_entity.Simulation_min_mounted
        max_possible_mounted = self.Credit_simulation_entity.Simulation_max_mounted
        print(f'max_possible_mounted:{max_possible_mounted}max_possible_mounted:{max_possible_mounted}')
        try:
            if Is_Validate_intervale(float(min_possible_mounted), float(max_possible_mounted),
                                     float(slot_value)) != True:
                raise Validators_exceptions.Validator_Intervale_Exception(
                    f"Please check the Amount value entered!!  \n Ammount must be between {min_possible_mounted} and {max_possible_mounted}")
            else:
                return {'mountedmoney': slot_value}
        except Validators_exceptions.Validator_Intervale_Exception as ex:
            self.Attempts=self.Attempts+1    
            ex.send_message(dispatcher=dispatcher)
        except ValueError as error:
            self.Attempts=self.Attempts+1    
            dispatcher.utter_message(text="Pleaze enter a valid  number  ")
        return {'mountedmoney': None}

    async def validate_period(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
                        domain: DomainDict) -> Dict[(Text, Any)]:
        """Validate Period value."""
        min_period = self.Credit_simulation_entity.periode_min
        max_period = self.Credit_simulation_entity.periode_max
        try:
            if Is_Validate_intervale(float(min_period), float(max_period), float(slot_value)) != True:
                raise Validators_exceptions.Validator_Intervale_Exception(
                    f"Please check the Period value entered  must be bettween {min_period} and {max_period} but your last entred value was {slot_value} ")
            else:
                return {'period': slot_value}
        except Validators_exceptions.Validator_Intervale_Exception as ex:
            ex.send_message(dispatcher=dispatcher)
        except ValueError as error:
            dispatcher.utter_message(text=f"Pleaze enter a valid  Value Like {random.randint(min_period,max_period)}  ")
        return {'period': None}
    
    
class Action_credit_calcultion(Action):
    """
    Action_credit_calcultion this class responsible for calculating the Credit


    Args:
        Action ([type]): Event
    """

    def name(self) -> Text:
        return 'action_submit_calcul_credit'

    async def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
        selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
        Credit_simulation = Get_Calcul_Credit_simulation(tracker, selected_simulation_id, slot_simulation_name)
        Result_calcul = calcul_credit_monthly(Credit_simulation.period, Credit_simulation.mounted_money,
                                              Credit_simulation.Simulation_added_value)
        print(Result_calcul)

        dispatcher.utter_message(text=f' You will pay every month {Result_calcul} DT ')
 