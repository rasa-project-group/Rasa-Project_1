from actions.DAO_Chatbot.Entity.Finance_entity import Finance_statistic_entity
from rasa_sdk.types import DomainDict
from actions.DAO_Chatbot.Entity.Simulations_entities import Credit_simulation, Deposit_simulation, simulation
from typing import Any, Optional, Text, Dict, List
import pandas as pd, datetime
from rasa_sdk import Action, Tracker
from actions.DAO_Chatbot.Entity.Chatbot_info_entity import chatbot_info_entity
from rasa_sdk.events import ActionExecuted, ConversationPaused, FollowupAction, SlotSet, EventType, SessionStarted, ReminderScheduled, BotUttered, UserUtteranceReverted,ActiveLoop
from actions.DAO_Chatbot.Implementation.ImpChatbot_offer import Implchatbot_offer
from rasa_sdk.executor import CollectingDispatcher
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions.return_exception import Empty_list_exception, Note_found_exception
from rasa_sdk.interfaces import ActionExecutionRejection
from actions.Helpers.Convert_helpers import Convert_string_datetime, convert_class_offers_to_json, convert_type_to_button, \
    convert_types_to_buttons, Convert_VariableName_to_String, convert_item_id_to_buttons,convert_data_to_rocketchatAttachment
from actions.Helpers.Extract_helpers import Extract_Simulation_types_list, Extract_offers_types_list, Extract_id_string
from actions.DAO_Chatbot.Implementation import ImpChatbot_info
from actions.DAO_Chatbot.Implementation.ImpSimulation_file import Imp_Simulation_credit, Impl_Simulations_Deposit
from actions.Helpers.Credits_calcul import calcul_credit_monthly, calcul_credit_yearly
from rasa_sdk.forms import FormAction, FormValidationAction, REQUESTED_SLOT
from actions.Helpers.Validators_helpers import Is_Valid_SameCurrency, Is_Validate_intervale, Is_valide_RocketChat_Channel, Is_valid_attempt
from actions.Cache.simulations_cache import Get_Current_selected_simulation_name_list, Get_calcul_Deposit_simulation, Get_selected_simulation, \
    Get_Calcul_Credit_simulation, Get_simulations_subtypes, cancel_asking_onSlot_element, cancel_simulation, reset_form_slots
from actions.DAO_Chatbot.Implementation.ImpAgency_location import Imp_Agency_location   
from actions.GoogleMap.entitiesMap import client_cordinates  
from actions.GoogleMap.Methodes import  Get_nearest_location
from rasa.core.channels.rocketchat import     RocketChatInput
from actions.Translater.Translate_message.Translate_utter_message import Translate_message_to_other_language
import datetime
from actions.DAO_Chatbot.Implementation.ImpFinance_currency_Grpc import Imp_finance_Curreny
import random
from  actions.actions_form.action_form_credit import Action_Form_credit_validation,Action_credit_calcultion
from  actions.actions_form.action_form_deposit import Action_Form_Deposit_validation,Action_Deposit_calcultion
from actions.actions_Slots_Questions.action_Slot_Q_credit import Action_ask_mounted_money, Action_ask_period
from actions.actions_Slots_Questions.action_Slot_Q_deposit import Action_ask_depositmoney,Action_ask_initialpayment,Action_ask_depositperiod
from actions.actions_form.action_form_currency import Action_validate_currency_form,Action_Call_currency
from actions.actions_Slots_Questions.actions_Slot_Q_currency import Action_ask_chosen_from_Currency,Action_ask_chosen_to_Currency
from actions.actions_form.action_form_currency_statistic import Action_validate_currency_statistic,Action_Call_statistic
from actions.actions_utils.actions_utils import  Action_afirm_deny,Action_Confirm,Action_Reset_offers_form,Ation_Reset_form_currrency,Ation_Reset_form_currrency_statistc
from rasa.core.channels import rocketchat

class action_routing_agent(Action):
    def name(self) -> Text:
        return "action_routing_agent"
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            confirm_submit=tracker.get_slot("confirm")
            if(confirm_submit==True):
                dispatcher.utter_message(text="Please hold on, this operation coul take some time .........")
                problem_title=tracker.get_slot("problem_title")
                dispatcher.utter_message(LiveChat_action=True ,LiveChat_custom_json={"action_name":"agent_routing","sender_id":tracker.sender_id,"problem_tag":problem_title})
            else:
                dispatcher.utter_message(text="Can i help you with some thing else !!!!")
        
        return []
    
class action_Notify_Client(Action):
    def name (self)->Text:
        return "action_notify_client"
    def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        print("notified")
        dispatcher.utter_message(LiveChat_action=True ,LiveChat_custom_json={"action_name":"notify_location"})
        
        return[]  
    
class action_agency_location(Action):
    def name(self)->Text:
        return "action_location_response"
    
    
    
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        latitude=tracker.get_slot("loactionlatitude")
        longtitude=tracker.get_slot("locationlongtitude")
        print(latitude)
        print(longtitude)
        
        imp_agency_locations=Imp_Agency_location("")
        agencies_locations=imp_agency_locations.Get_Agency_locations()
        addresses=[item.agency_address for item in agencies_locations]
        cordinates= client_cordinates()
        cordinates.lat=latitude
        cordinates.lng=longtitude
    
        Final_adress=Get_nearest_location(cordinates,addresses)
        print(Final_adress)
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            Rocket_Attachment=convert_data_to_rocketchatAttachment("mapcard",attachment_data=Final_adress)
            dispatcher.utter_message(text=f"here all the nearest places according to your current position",attachment=Rocket_Attachment)
        return []      