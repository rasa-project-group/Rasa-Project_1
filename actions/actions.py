from actions.DAO_Chatbot.Entity.Finance_entity import Finance_statistic_entity
from rasa_sdk.types import DomainDict
from actions.DAO_Chatbot.Entity.Simulations_entities import Credit_simulation, Deposit_simulation, simulation
from typing import Any, Optional, Text, Dict, List
import pandas as pd, datetime
from rasa_sdk import Action, Tracker
from actions.DAO_Chatbot.Entity.Chatbot_info_entity import chatbot_info_entity
from rasa_sdk.events import ActionExecuted, ConversationPaused, FollowupAction, SlotSet, EventType, SessionStarted, ReminderScheduled, BotUttered, UserUtteranceReverted,ActiveLoop
from actions.DAO_Chatbot.Implementation.ImpChatbot_offer import Implchatbot_offer
from rasa_sdk.executor import CollectingDispatcher
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions import Validators_exceptions
from actions.DAO_Chatbot.Exceptions.return_exception import Empty_list_exception, Note_found_exception
from rasa_sdk.interfaces import ActionExecutionRejection
from actions.Helpers.Convert_helpers import Convert_string_datetime, convert_class_offers_to_json, convert_type_to_button, \
    convert_types_to_buttons, Convert_VariableName_to_String, convert_item_id_to_buttons,convert_data_to_rocketchatAttachment
from actions.Helpers.Extract_helpers import Extract_Simulation_types_list, Extract_offers_types_list, Extract_id_string
from actions.DAO_Chatbot.Implementation import ImpChatbot_info
from actions.DAO_Chatbot.Implementation.ImpSimulation_file import Imp_Simulation_credit, Impl_Simulations_Deposit
from actions.Helpers.Credits_calcul import calcul_credit_monthly, calcul_credit_yearly
from rasa_sdk.forms import FormAction, FormValidationAction, REQUESTED_SLOT
from actions.Helpers.Validators_helpers import Is_Valid_SameCurrency, Is_Validate_intervale, Is_valide_RocketChat_Channel, Is_valid_attempt
from actions.Cache.simulations_cache import Get_Current_selected_simulation_name_list, Get_calcul_Deposit_simulation, Get_selected_simulation, \
    Get_Calcul_Credit_simulation, Get_simulations_subtypes, cancel_asking_onSlot_element, cancel_simulation, reset_form_slots
from actions.DAO_Chatbot.Implementation.ImpAgency_location import Imp_Agency_location   
from actions.GoogleMap.entitiesMap import client_cordinates  
from actions.GoogleMap.Methodes import  Get_nearest_location
from rasa.core.channels.rocketchat import     RocketChatInput
from actions.Translater.Translate_message.Translate_utter_message import Translate_message_to_other_language
import datetime
from actions.DAO_Chatbot.Implementation.ImpFinance_currency_Grpc import Imp_finance_Curreny
import random
from  actions.actions_form.action_form_credit import Action_Form_credit_validation,Action_credit_calcultion
from  actions.actions_form.action_form_deposit import Action_Form_Deposit_validation,Action_Deposit_calcultion
from actions.actions_Slots_Questions.action_Slot_Q_credit import Action_ask_mounted_money, Action_ask_period
from actions.actions_Slots_Questions.action_Slot_Q_deposit import Action_ask_depositmoney,Action_ask_initialpayment,Action_ask_depositperiod
from actions.actions_form.action_form_currency import Action_validate_currency_form,Action_Call_currency
from actions.actions_Slots_Questions.actions_Slot_Q_currency import Action_ask_chosen_from_Currency,Action_ask_chosen_to_Currency
from actions.actions_form.action_form_currency_statistic import Action_validate_currency_statistic,Action_Call_statistic
from actions.actions_utils.actions_utils import  Action_afirm_deny,Action_Confirm,Action_Reset_offers_form,Ation_Reset_form_currrency,Ation_Reset_form_currrency_statistc
from actions.Actions_External.Action_External_Rocketchat import action_routing_agent,action_agency_location,action_Notify_Client
from rasa.core.channels import rocketchat
#region Class Action HellowWorld 
class ActionHelloWorld(Action):
    
    def name(self) -> Text:
        return 'action_hello_world'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        dispatcher.utter_message(text='Hello World!')
        return []
#endregion  


#region Class Action language has changed
class Action_language_has_changed(Action):
      
    def name(self) -> Text:
        return 'action_language_has_changed'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        introduce_message=tracker.get_slot("Bot_introduce")
        chosen_language=tracker.get_slot("chosen_language")
 
        translated_introduce=Translate_message_to_other_language(chosen_language,introduce_message,"en")
        dispatcher.utter_message(text=translated_introduce)
        return []
#endregion    
    
    
#region Class Action Session start     


class ActionSessionStart(Action):

    def name(self) -> Text:
        return 'action_session_start'

    @staticmethod
    def fetch_slots(tracker: Tracker) -> List[EventType]:
        """This method retrieves all the data necessary to start or restart the session.

        Args:
           tracker (Tracker): Acces to Slots 
           slots   (List[str]): Slots that will recovered

        Returns:
            List[EventType]:  Event that will be recovered
        """
        slots = []
        
        _Imp_cahtbot = ImpChatbot_info.Imp_Chatbot_info('Noting_For_now')
        _Imp_offers = Implchatbot_offer('Noting_For_now')
        _Imp_Simulation_credit = Imp_Simulation_credit('')
        _Imp_Simulation_deposit = Impl_Simulations_Deposit('')
        list_offers = _Imp_offers.Get_Chatbot_offers()
        Chatbot_info_entity = _Imp_cahtbot.Get_Chatbot_info()
        Creditl_simulation_list = _Imp_Simulation_credit.Get_simulations_Credit()
        Deposit_simulation_list = _Imp_Simulation_deposit.Get_simulations_Deposit()
        slots.append(SlotSet(key='Bot_Name', value=(Chatbot_info_entity.name)))
        slots.append(SlotSet(key='Bot_Age', value=(Chatbot_info_entity.Age)))
        slots.append(SlotSet(key='Bot_Gender', value=(Chatbot_info_entity.Gender)))
        slots.append(SlotSet(key='Bot_Lastname', value=(Chatbot_info_entity.lastname)))
        slots.append(SlotSet(key='Bot_introduce', value=(Chatbot_info_entity.introduce)))
        slots.append(SlotSet(key='offerlist', value=list_offers))
        slots.append(SlotSet(key='simulation_credit_list', value=Creditl_simulation_list))
        slots.append(SlotSet(key='simulation_deposit_list', value=Deposit_simulation_list))
        slots.append(SlotSet(key='chosen_language', value="en"))

        for key in ('Bot_Name', 'Bot_Age', 'Bot_Gender', 'Bot_Lastname', 'offerlist', 'simulation_credit_list',
                    'simulation_deposit_list',"chosen_language"):
            value = tracker.get_slot(key)
            if value is not None:
                slots.append(SlotSet(key=key, value=value))

        return slots

    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[Dict[(Text, Any)]]:
        slots_fetched = self.fetch_slots(tracker)
        language_list=[{"title_language":"French","symbol":"fr"},{"title_language":"Arabic","symbol":"ar"}]
        """list of the disposable language"""
        metadata = tracker.get_slot("session_started_metadata")
        print("this meta data")
        print(metadata)
        language_buttons=[convert_type_to_button(item["title_language"],"chosen_language",item["symbol"],"available_language")   for item in language_list ]
        introduce_message = slots_fetched[4]['value']
        print(tracker.get_latest_input_channel())
        print(language_buttons)
        Welcome_text="👋"+introduce_message+"\nThe default language to answer your question is English!! you can chose one those "  
        print("tracker name")
        
        print(tracker.get_latest_input_channel())

        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            print(tracker.get_latest_input_channel())
             
            Buttons_attachaments=convert_data_to_rocketchatAttachment("buttons",attachment_data=language_buttons)
            dispatcher.utter_message(text=Welcome_text,attachment=Buttons_attachaments )
        else:
            dispatcher.utter_message(text=Welcome_text, buttons=language_buttons)
        events = [SessionStarted()]
        events.append(ActionExecuted('action_listen'))
        events.extend(slots_fetched)
        return events 
    
class Action_show_offer_type(Action):
    _Imp_cahtbot = ImpChatbot_info.Imp_Chatbot_info('Noting_For_now')
    _Chatbot_info_entity = _Imp_cahtbot.Get_Chatbot_info()
    def name(self) -> Text:
        return 'action_show_offer_type'
    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:

    
        Offer_from_slot = tracker.get_slot('offerlist')
        offers_types = Extract_offers_types_list(Offer_from_slot)
        Message_text=f"Of course! Our Bank provide {len(offers_types)} categories of offers and products.\n Here are they 👇"
        
        Rep_messages=["Ok! it seems like you are interrested in what we have.\n here are the offers categories","2"]
        offers_buttons = convert_types_to_buttons(offers_types, slot_name='selectedoffertype', intent_name='show_offer_content')
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            RocketChat_attachments=convert_data_to_rocketchatAttachment("buttons",attachment_data=offers_buttons)
            dispatcher.utter_message(text=f"{Message_text}",attachment=RocketChat_attachments)       

            dispatcher.utter_message(text=f"Please select one of them")           
        else:

            dispatcher.utter_message(buttons=offers_buttons,text=f"These are all the offers we have at the moment")
            
        return []

#endregion


#region Class Action Show offers
class Action_Show_offer(Action):

    def name(self) -> Text:
        return 'action_show_offer_content'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        Offers_from_slot = tracker.get_slot('offerlist')
        
        Selected_offer = tracker.get_slot('selectedoffertype')
        
        
  
        
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            
            offers_by_types_list = [ offer for offer in Offers_from_slot if offer['offer_type'] == Selected_offer]
            
            dispatcher.utter_message( text=f"Here are our offers corresponding to the {Selected_offer} type \n",attachment={"attachments_type":"offers","attachments_data":offers_by_types_list})
                 
        else:
           offers_by_types_list = [{'name': offer_by_type['offer_title'], 'id': offer_by_type['offer_id']} for
                                   offer_by_type in Offers_from_slot if offer_by_type['offer_type'] == Selected_offer]
           Buttons = convert_item_id_to_buttons(offers_by_types_list, 'selectedofferdeatils',
                                                intent_name='show_offer_details')
           dispatcher.utter_message(buttons=Buttons,
                                    text=f"Here are our offers corresponding to the {Selected_offer} type \n")
        return [FollowupAction("action_reset_offer_form")]
#endregion

#region Class Action Show offers details
class Action_show_offers_details(Action):

    def name(self) -> Text:
        return 'action_show_offer_details'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        Selected_offer_id = tracker.get_slot('selectedofferdeatils')
        Offers_from_slot = tracker.get_slot('offerlist')
        Select_offer_info = [offe_details for offe_details in Offers_from_slot if
                             offe_details['offer_id'] == Selected_offer_id]
        offer_title = Select_offer_info[0]['offer_title']
        offer_subtitle = Select_offer_info[0]['offer_subtitle']
        offer_description = Select_offer_info[0]['offer_description']
        offer_image = Select_offer_info[0]['offer_Image_path']
        Result = '\n' + offer_title + '\n' + offer_subtitle + '\n' + offer_description
        dispatcher.utter_message(text=Result, image=offer_image)
        return []
#endregion

#region Class Action Ask to continue
class Action_ask_continue(Action):
    def name(self) -> Text:
        return 'action_ask_continue'
    
    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:

        dispatcher.utter_message(text="Would you like to continue this operation")
        return []
#endregion    

#region Class Action Show simulation types(Credit or Deposit)
class Action_show_simulations_types(Action):

    def name(self) -> Text:
        return 'action_show_simulation_type'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        simulations_types = ["Deposit","Credit"]
        # simulation_credit_list = tracker.get_slot('simulation_credit_list')
        # simulation_deposit_list = tracker.get_slot('simulation_deposit_list')
        # extractions_types = Extract_Simulation_types_list(simulation_credit_list, '')
        # extractions_types.update(Extract_Simulation_types_list(simulation_deposit_list, ''))
        
        All_buttons_list = [convert_type_to_button(item_type_simulation, 'selectedsimulation', item_type_simulation,
                                                  'show_simulations_content_' + str.upper(item_type_simulation)) for
                           item_type_simulation in simulations_types]        
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            
            print(All_buttons_list)
            
            RocketChat_attachments=convert_data_to_rocketchatAttachment("simulations",attachment_data=All_buttons_list)
            dispatcher.utter_message(text="These all simulations that we have",attachment=RocketChat_attachments)
            
        else:    
            dispatcher.utter_message(text='You can select the simulation type tha you want \n', buttons=All_buttons_list)
        return []
#endregion

#region Class Action show simulation subtypes(Category of Credit ...)
class Action_show_simulations_subtype(Action):
    def name(self) -> Text:
            return 'action_show_simulation_subtype'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        All_buttons_list = []
        
        list_name = Get_Current_selected_simulation_name_list(tracker)
        list_simulation = tracker.get_slot(list_name)
        print(list_simulation)

        list_types=Extract_Simulation_types_list(list_simulation,"")
        print("error here")

        print(list_types)
        if(len(list_types)==1):
        
            dispatcher.utter_message(text=f"There is only one type :\n {list_types[0]}  ")
            return [FollowupAction("action_show_simulation_content")] 
        
        All_buttons_list = convert_types_to_buttons(list_types,"selectedsubsimulation","show_simulation_subtype")

        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            print(All_buttons_list)
            
            RocketChat_attachments=convert_data_to_rocketchatAttachment("simulationsubtypes",attachment_data=All_buttons_list)
            dispatcher.utter_message(text='Ofcur', attachment=RocketChat_attachments)
            return []
        
        else:
            dispatcher.utter_message(text='simulations subtypes', buttons=All_buttons_list)
            return []
#endregion     






#region Class Action Show simulation content

class Action_show_simulations_content(Action):

    def name(self) -> Text:
        return 'action_show_simulation_content'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
        sub_type=tracker.get_slot("selectedsubsimulation")
        try:
           simulation_list = Get_simulations_subtypes(sub_type,slot_simulation_name,tracker)
           Simulations = [{'name': simulation['simulation_name'], 'id': simulation['simulation_id']} for simulation in simulation_list]
           buttonss = convert_item_id_to_buttons(Simulations, 'selectedsimulationdetails', 'show_simulation_details')
           if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
                print(buttonss)
                Rocketchat_attachment=convert_data_to_rocketchatAttachment("buttons",buttonss)
                dispatcher.utter_message(text=f"You have selected the type of {sub_type} ")
                dispatcher.utter_message(text='',attachment=Rocketchat_attachment)
                dispatcher.utter_message(text="please select one of them 👆")

        except Empty_list_exception as ex:
            ex.send_message(dispatcher)
        return []

#endregion

#region Class Action Show simulation details 

class Action_show_simulations_details(Action):

    def name(self) -> Text:
        return 'action_simulation_details'

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
        Dict[(Text, Any)]]:
        slot_simulation_list_name = Get_Current_selected_simulation_name_list(tracker)
        selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
        selected_simulation_type = tracker.get_slot('selectedsimulation')
        try:
            simul_entity = Get_selected_simulation(tracker, selected_simulation_id, slot_simulation_list_name,
                                                   selected_simulation_type)
            message = str(simul_entity.simulation_name + '\n- period min is : ' + str(
                simul_entity.periode_min) + '\n- period max is ' + str(simul_entity.periode_max))
            buttonss = [{'title': 'yes', 'payload': '/affirm'}, {'title': 'No', 'payload': '/deny'}]
            if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
                RocketChat_Atachment=convert_data_to_rocketchatAttachment("buttons",attachment_data=buttonss)
                dispatcher.utter_message(text=f"Pleaze set yes or no to run this simulations",attachment=RocketChat_Atachment)
            else:
                dispatcher.utter_message(text=message, buttons=buttonss)
    
            return []
        except Empty_list_exception as ex:
            ex.send_message(dispatcher=dispatcher)
        except Note_found_exception as ex:
            ex.send_message()
        except KeyError as error:
            dispatcher.utter_message(text="There is problem pleaze try agin")
        return []

#endregion

# #region Class Action ask mounted money
# class Action_ask_mounted_money(Action):

#     def name(self) -> Text:
#         return 'action_ask_mountedmoney'

#     def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
#         Dict[(Text, Any)]]:
#         current_selected_slote=tracker.get_slot("requested_slot")
#         print(current_selected_slote)
#         if(current_selected_slote=="mountedmoney"): #verify if currrent requested slote equal to deposit money
#             return  []
                    
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         credit_simulation: Credit_simulation = Get_selected_simulation(tracker, selected_simulation_id,
#                                                                        slot_simulation_name, "Credit")

#         dispatcher.utter_message(
#             text=f"The credit ammount  for the  {credit_simulation.simulation_name}  must be between: {credit_simulation.Simulation_min_mounted} and {credit_simulation.Simulation_max_mounted} ")
#         return []

# #endregion

# #region Class Action Ask period
# class Action_ask_period(Action):

#     def name(self) -> Text:
#         return 'action_ask_period'

#     def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
#         Dict[(Text, Any)]]:
#         current_selected_slote=tracker.get_slot("requested_slot")
#         if(current_selected_slote=="period"):
#             return []
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         selectect_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         credit_simulation: Credit_simulation = Get_selected_simulation(tracker=tracker,
#                                                                        selected_simulation_id=selectect_simulation_id,
#                                                                        list_simulation_slot_name=slot_simulation_name)
#         dispatcher.utter_message(
#             text=f" The period credit  payment for the {credit_simulation.simulation_name} credit must be between: {credit_simulation.periode_min} and {credit_simulation.periode_max} ")
#         return []

# #endregion

#region  All Action Classes  For Deposit Ask

# class Action_ask_initialpayment(Action):

#     def name(self) -> Text:
#         return 'action_ask_initialpayment'

#     def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
#         Dict[(Text, Any)]]:
#         current_selected_slote=tracker.get_slot("requested_slot")
#         if(current_selected_slote=="initialpayment"):
#             return []        
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         Desposit_simulation:Deposit_simulation = Get_selected_simulation(tracker, selected_simulation_id, slot_simulation_name, 'deposit')
#         print(Desposit_simulation.initial_deposit_max)
#         dispatcher.utter_message(
#             text=f" The  initial deposit for the {Desposit_simulation.simulation_name} must be between{Desposit_simulation.initial_deposit_min} and {Desposit_simulation.initial_deposit_max}   ")
#         return []

# class Action_ask_depositmoney(Action):

#     def name(self) -> Text:
#         return 'action_ask_despositmoney'

#     def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
#         Dict[(Text, Any)]]:
#         current_selected_slote=tracker.get_slot("requested_slot")
#         if(current_selected_slote=="depositmoney"):
#             return []    
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         Desposit_simulation: Deposit_simulation = Get_selected_simulation(tracker, selected_simulation_id,
#                                                                           slot_simulation_name, 'deposit')
#         dispatcher.utter_message(
#             text=f" The deposit payment for the  {Desposit_simulation.simulation_name} Deposit must be between  {Desposit_simulation.Simulation_min_mounted}   and  {Desposit_simulation.Simulation_max_mounted}   ")
#         return []


# class Action_ask_depositperiod(Action):

#     def name(self) -> Text:
#         return 'action_ask_perioddeposit'

#     def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
#         Dict[(Text, Any)]]:
#         current_selected_slote=tracker.get_slot("requested_slot")
#         if(current_selected_slote=="perioddeposit"):
#             return []         
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         Desposit_simulation: Deposit_simulation = Get_selected_simulation(tracker, selected_simulation_id,
#                                                                           slot_simulation_name, 'deposit')
#         dispatcher.utter_message(
#             text=f" The  Deposit period payment for the  {Desposit_simulation.simulation_name} deposit must be between {Desposit_simulation.periode_min} and {Desposit_simulation.periode_max}   ")
#         return []

# #endregion

#region  All Action Classes  Calculation 


# class Action_credit_calcultion(Action):
#     """
#     Action_credit_calcultion this class responsible for calculating the Credit


#     Args:
#         Action ([type]): Event
#     """

#     def name(self) -> Text:
#         return 'action_submit_calcul_credit'

#     async def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
#         Dict[(Text, Any)]]:
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         Credit_simulation = Get_Calcul_Credit_simulation(tracker, selected_simulation_id, slot_simulation_name)
#         Result_calcul = calcul_credit_monthly(Credit_simulation.period, Credit_simulation.mounted_money,
#                                               Credit_simulation.Simulation_added_value)
#         print(Result_calcul)

#         dispatcher.utter_message(text=f' You will pay every month {Result_calcul} DT ')
#         return []
    
    
    
# class Action_Deposit_calcultion(Action):
#     """
#     Action_Deposit_calcultion this class responsible for calculating the deposit
    
#     Args:
#         Action ([type]):  Event 
#     """
    
#     def name(self) -> Text:
#         return 'action_submit_calcul_deposit'

#     async def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[(Text, Any)]) -> List[
#         Dict[(Text, Any)]]:
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         selected_simulation_id = tracker.get_slot('selectedsimulationdetails')

#         dispatcher.utter_message(text=f' You will pay every month 55555 DT ')
#         return reset_form_slots(["initialpayment","despositmoney","perioddeposit"])


#endregion

#region  All Validations Action    (Simulations)  

# class Action_Form_credit_validation(FormValidationAction):
#     selected_simulation_id: str = None
#     Credit_simulation_entity:Credit_simulation
#     Counter=0
    
#     Max_attempts=3;
#     Attempts=0;
#     async def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: DomainDict) -> List[EventType]:
#         Events_listners=[]
#         Events_listners=cancel_simulation(dispatcher,tracker)

#         if(len(Events_listners)==0):
#             print(Events_listners)
#             print("dibne")
#             return  await super().run(dispatcher, tracker, domain)
#         else: 
            
#             if(self.Counter==0):
#                 dispatcher.utter_message(text=f"You have canceled the  simulation process    and you can restart the operation if you wish.")
#                 print(self.Counter)
#                 self.Counter=self.Counter+1
#             return [SlotSet("requested_slot",None)]       

#     def name(self) -> Text:
#         return 'validate_calculcredit_form'



#     async def validate_mountedmoney(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                               domain: DomainDict) -> Dict[(Text, Any)]:
#         """Validate mountedmoney value."""
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         self.selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         print(slot_simulation_name)
#         self.Credit_simulation_entity = Get_selected_simulation(tracker, self.selected_simulation_id, slot_simulation_name)
#         sol1=Get_selected_simulation(tracker, self.selected_simulation_id, slot_simulation_name)
#         print(f'max_possible_mounted:{sol1.Simulation_max_mounted},min:{sol1.Simulation_min_mounted}')
        
#         min_possible_mounted = self.Credit_simulation_entity.Simulation_min_mounted
#         max_possible_mounted = self.Credit_simulation_entity.Simulation_max_mounted
#         print(f'max_possible_mounted:{max_possible_mounted}max_possible_mounted:{max_possible_mounted}')
        
        
        
#         try:
        
   
            
#             if Is_Validate_intervale(float(min_possible_mounted), float(max_possible_mounted),
#                                      float(slot_value)) != True:
#                 raise Validators_exceptions.Validator_Intervale_Exception(
#                     f"Please check the Amount value entered!!  \n Ammount must be between {min_possible_mounted} and {max_possible_mounted}")
#             else:
#                 return {'mountedmoney': slot_value}
#         except Validators_exceptions.Validator_Intervale_Exception as ex:
#             self.Attempts=self.Attempts+1    
#             ex.send_message(dispatcher=dispatcher)
#         except ValueError as error:
#             self.Attempts=self.Attempts+1    
#             dispatcher.utter_message(text="Pleaze enter a valid  number  ")
#         return {'mountedmoney': None}

#     async def validate_period(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                         domain: DomainDict) -> Dict[(Text, Any)]:
#         """Validate Period value."""
#         min_period = self.Credit_simulation_entity.periode_min
#         max_period = self.Credit_simulation_entity.periode_max
#         try:
#             if Is_Validate_intervale(float(min_period), float(max_period), float(slot_value)) != True:
#                 raise Validators_exceptions.Validator_Intervale_Exception(
#                     f"Please check the Period value entered  must be bettween {min_period} and {max_period} but your last entred value was {slot_value} ")
#             else:
#                 return {'period': slot_value}
#         except Validators_exceptions.Validator_Intervale_Exception as ex:
#             ex.send_message(dispatcher=dispatcher)
#         except ValueError as error:
#             dispatcher.utter_message(text=f"Pleaze enter a valid  Value Like {random.randint(min_period,max_period)}  ")
#         return {'period': None}
 
 

# class Action_Form_Deposit_validation(FormValidationAction):
#     """
#     Action_Form_Deposit_validation this class validate the Deposit  Simulation Form 

#     [extended_summary]

#     Args:
#         FormValidationAction ([type]): [description]

#     Raises:
#         Validators_exceptions.Validator_Intervale_Exception: [description]
#         Validators_exceptions.Validator_Intervale_Exception: [description]
#         Validators_exceptions.Validator_Intervale_Exception: [description]

#     Returns:
#         [type]: [description]
#     """
#     selected_simulation_id: str = None
#     Deposit_simulation_entity: Deposit_simulation = None
    

    
#     async def run(self, dispatcher: "CollectingDispatcher", tracker: "Tracker", domain: "DomainDict") -> List[EventType]:

#         Event_list=cancel_simulation(dispatcher,tracker)
#         if(len(Event_list)==0):
#             return await super().run(dispatcher, tracker, domain) 
#         else: return Event_list + reset_form_slots(self.slots_mapped_in_domain(domain))       
        
#     def name(self) -> Text:
#         return 'validate_calculdeposit_form'
#     async def required_slots(self,slots_mapped_in_domain : List[Text], dispatcher: "CollectingDispatcher", tracker: "Tracker", domain: "DomainDict") -> List[Text]:
 
#         return slots_mapped_in_domain        

#     async def validate_initialpayment(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                 domain: DomainDict) -> Dict[(Text, Any)]:
#         """
#         validate_initialpayment Validate the initial payment value


#         Raises:
#             Validators_exceptions.Validator_Intervale_Exception: 

#         Returns:
#             [type]: [description]
#         """
#         slot_simulation_name = Get_Current_selected_simulation_name_list(tracker)
#         self.selected_simulation_id = tracker.get_slot('selectedsimulationdetails')
#         print(slot_simulation_name)
#         self.Deposit_simulation_entity:Deposit_simulation = Get_selected_simulation(tracker, self.selected_simulation_id,
#                                                                  slot_simulation_name,"Deposit")
#         min_initial_deposit = self.Deposit_simulation_entity.initial_deposit_min
#         max_initial_deposit = self.Deposit_simulation_entity.initial_deposit_max
#         try:
#             if Is_Validate_intervale(float(min_initial_deposit), float(max_initial_deposit), float(slot_value)) != True:
#                 raise Validators_exceptions.Validator_Intervale_Exception(
#                     f"Please check the Amount value entered!!  ")
#             else:
#                 return {'initialpayment': slot_value}
#         except Validators_exceptions.Validator_Intervale_Exception as ex:
#             ex.send_message(dispatcher=dispatcher)
#         except ValueError as error:
#             dispatcher.utter_message(text="Pleaze enter a valid  number  ")
#         return {'initialpayment': None}

#     async def validate_despositmoney(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
#         """Validate despositmoney value."""
#         min_period = self.Deposit_simulation_entity.Simulation_min_mounted
#         max_period = self.Deposit_simulation_entity.Simulation_max_mounted
#         try:
#             if Is_Validate_intervale(float(min_period), float(max_period), float(slot_value)) != True:
#                 raise Validators_exceptions.Validator_Intervale_Exception(
#                     f"Please check the Period value entered ")
#             else:
#                 return {'despositmoney': slot_value}
#         except Validators_exceptions.Validator_Intervale_Exception as ex:
#             ex.send_message(dispatcher=dispatcher)
#         except ValueError as error:
#             dispatcher.utter_message("Pleaze enter a valid  number  ")
#         return {'despositmoney': None}

#     async def validate_perioddeposit(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
#         """Validate perioddeposit value."""
#         min_period = self.Deposit_simulation_entity.periode_min
#         max_period = self.Deposit_simulation_entity.periode_max
#         try:
#             if Is_Validate_intervale(float(min_period), float(max_period), float(slot_value)) != True:
#                 raise Validators_exceptions.Validator_Intervale_Exception(
#                     f"Please check the Period value entered ")
#             else:
#                 return {'perioddeposit': slot_value}
#         except Validators_exceptions.Validator_Intervale_Exception as ex:
#             ex.send_message(dispatcher=dispatcher)
#         except ValueError as error:
#             dispatcher.utter_message(text="Pleaze enter a valid  number  ")
#         return {'perioddeposit': None}
#endregion

#region All  Action finance Currency
#region simple currency convertor

# class Action_ask_chosen_from_Currency(Action):
#     def name(self) -> Text:
#         return "action_ask_fromcurrency"
    
#     def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        
#         dispatcher.utter_message("Pleaze set the 1st currency symbols like:\n-TND\n-USD\n-EUR\n...")
#         return []
# class Action_ask_chosen_to_Currency(Action):
#     def name(self) -> Text:
#         return "action_ask_tocurrency"
    
#     def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        
#         dispatcher.utter_message("Pleaze set the 2st currency symbols like:\n-TND\n-USD\n-EUR\n... ")
#         return []
# class Action_Call_currency(Action):
#     def name(self) -> Text:

#         return "action_convert_currency"
    
#     def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         currency_imp=Imp_finance_Curreny()
#         from_currency:str=tracker.get_slot("fromcurrency")
#         to_currency:str=tracker.get_slot("tocurrency")
#         result=currency_imp.Convert_currency(from_currency,to_currency)   
    
#         dispatcher.utter_message(text=f"The conversion From {result.From_Currency_Name} to {result.To_Currency_Name} :\n"
#                                   +f'- The Exchange rates       : {"{:.6f}".format(result.Exchange_Rate)}\n'
#                                  + f'- The Bid price            : {"{:.6f}".format(result.Bid_price)}\n'
#                                  + f'- The Ask price            : {"{:.6f}".format(result.Ask_price)}\n'
#                                  + f'- The conversion date      : {Convert_string_datetime(result.Last_Refreshed,"%b %d %Y %H:%M:%S" )}\n'
#                                  )
#         return [FollowupAction("action_reset_form_currency_statistic")]         
    
# class Action_validate_currency_form(FormValidationAction):
#     def name(self) -> Text:
#         return "validate_finance_form"
#     def validate_fromcurrency(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
#         return {"fromcurrency":slot_value}
#     def validate_tocurrency(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
        
  
#         return {"tocurrency":slot_value}
      
#endregion    
# class Action_ask_columns_name(Action):
#     Fixed_columns=["close","high","open","low"]
    
#     def name(self) -> Text:
#         return "action_ask_elementlist"
    
#     def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         plots_names:List=tracker.get_slot("ColumnsCurrencyExchange")
#         if(plots_names!=None ):
#             setA = set(plots_names)
#             setB = set(self.Fixed_columns)
#             Res=setB -setA
#             message:str=""
#             for ele in Res :
#                 message=message+"\n"+ele
#             print(message)    
#             dispatcher.utter_message(text=f"What type of data are you looking for: {message}\n you can stop the filling by sayin 'stop filling list'")
#             return []

#         else:
#             dispatcher.utter_message(text=f"What type of data are you looking for: \n-close \n-open \n-low \n-high \n")
#             return []

            
# class Action_ask_data_date(Action):
#     def name(self) -> Text:
#         return "action_ask_DateCurrencyType"
#     def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         dispatcher.utter_message(text=f"pleaze enter the statistic date :\n-daily\n-weekly\n-monthly\n-interval")
#         return []        
# class Action_ask_intervale(Action):
#     def name(self) -> Text:
#         return "action_ask_intervalduration"
#     def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         dispatcher.utter_message(text=f"the interval could be for :\n1min\n15min\n30min\n60min")
#         return []         
# class Action_validate_currency_statistic(FormValidationAction):
#     additional_slots = []
#     Fixed_columns=["close","high","open","low"]
    

#     def name(self) -> Text:
#         return "validate_currencyExchangestatistic_form"
#     async def required_slots(self, slots_mapped_in_domain: List[Text], dispatcher: "CollectingDispatcher", tracker: "Tracker", domain: "DomainDict") -> List[Text]:
#         if tracker.slots.get("DateCurrencyType") == "interval":
#             self.additional_slots.append("intervalduration")
#             print(slots_mapped_in_domain)
#         return  slots_mapped_in_domain+ self.additional_slots 
#     async def extract_intervalduration(
#         self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
#     ) -> Dict[Text, Any]:
#         text_of_last_user_message = tracker.latest_message.get("text")
#         if(text_of_last_user_message not in ["15min","1min","30min","60min"]):
            
#             return {"intervalduration": None}
#         else:
#             return {"intervalduration": text_of_last_user_message}

#     async def validate_intervalduration(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
#         return {"intervalduration":slot_value}
    
#     async def validate_fromcurrency(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
#         return {"fromcurrency":slot_value}
    
#     async def validate_tocurrency(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
#         from_currency=tracker.get_slot("fromcurrency")
#         if(Is_Valid_SameCurrency(slot_value,from_currency)):
            
#             return {"tocurrency":slot_value}
#         else:return {"tocurrency":None}
        
#     async def validate_elementlist(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
#         if cancel_asking_onSlot_element(dispatcher,tracker,"stop_filling_list") :
            
#             return {'elementlist':slot_value} 
#         else:
#             selected_column_list:List=tracker.get_slot("ColumnsCurrencyExchange") or []
#             if(selected_column_list==None):
#                 selected_column_list=[]
#             if(len(selected_column_list)==3):
#                 selected_column_list.append(slot_value)
                
#                 return {'elementlist':slot_value,"ColumnsCurrencyExchange":selected_column_list}
#             elif(len(selected_column_list)<4):     
                
#                 if(slot_value  in selected_column_list ):
#                     dispatcher.utter_message(text=f"you have already chosed this {slot_value} ")
#                     return {'elementlist':None}
#                 else:
#                     selected_column_list.append(slot_value)
#                     return {'elementlist':None,"ColumnsCurrencyExchange":selected_column_list}

#     async def  validate_DateCurrencyType(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker,
#                                domain: DomainDict) -> Dict[(Text, Any)]:
        
#         return {"DateCurrencyType":slot_value}
# class Action_Call_statistic(Action):
#     def name(self) -> Text:
#         return "action_convert_statistic_currency"
#     async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         statistic_imp=Imp_finance_Curreny()
#         plots:List=tracker.get_slot("ColumnsCurrencyExchange")
#         from_currency=tracker.get_slot("fromcurrency")
#         to_currency=tracker.get_slot("tocurrency")
#         Data_type=tracker.get_slot("DateCurrencyType")
#         Interval:str=tracker.get_slot("intervalduration")
#         if(Interval==None):
#             Statistic_res:Finance_statistic_entity=statistic_imp.Get_Currency_details_Image(from_currency,to_currency,20,20,plots,Data_type=Data_type)
#         else:
#             Statistic_res:Finance_statistic_entity=statistic_imp.Get_Currency_detail_image_interval(from_currency,to_currency,20,20,plots,Data_type=Data_type,interval_date=tracker.get_slot("intervalduration"))
#             print(Statistic_res.Image_base64)
            
            
        
#         dispatcher.utter_message(text=Statistic_res.image_detail.Image_title,image=Statistic_res.Image_base64)
#         return [FollowupAction("action_reset_form_currency_statistic")]         
#endregion 




#region utils region
# class Action_afirm_deny(Action):
#     def name(self)->Text:
#         return "action_affirm_deny"
#     async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         buttons = [{'title': 'yes', 'payload': '/affirm'}, {'title': 'No', 'payload': '/deny'}]
        
#         if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
#             RocketChat_Message=convert_data_to_rocketchatAttachment("buttons",attachment_data=buttons)
#             dispatcher.utter_message(text="Pleaze confirm this operation",attachment=RocketChat_Message)
            
#         else:
#             dispatcher.utter_message(text="Pleaze confirm this operation",buttons=buttons)
#         return []
    
# class Action_Confirm(Action):
#     def name(self)->Text:
#         return "action_ask_confirm"
#     async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         buttons = [{'title': 'yes', 'payload': '/affirm'}, {'title': 'No', 'payload': '/deny'}]
        
#         if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
#             RocketChat_Message=convert_data_to_rocketchatAttachment("buttons",attachment_data=buttons)
#             dispatcher.utter_message(text="to reach an agent 🧑‍🔧 you must confirm (Yes or No) this operation",attachment=RocketChat_Message)
            
            
#         else:
#             dispatcher.utter_message(text="Pleaze confirm this operation",buttons=buttons)
#         return []    
# class Action_Reset_offers_form(Action):
#     def name(self)->Text:
#         return "action_reset_offer_form"
#     slots_names=["selected_type","selectedofferdeatils"]
    
#     async def run (self, dispatcher, tracker: Tracker, domain: "DomainDict")-> List[Dict[Text, Any]]:

#         Reset_Events=reset_form_slots(self.slots_names)
#         return Reset_Events


# class Ation_Reset_form_currrency(Action):
#     def name(self)->Text:
#         return "action_reset_form_currency"     
#     slots_names=["tocurrency","fromcurrency"]
           
#     async def run(self, dispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         Reset_Events=reset_form_slots(self.slots_names)
        
#         return Reset_Events       
# class Ation_Reset_form_currrency_statistc(Action):
#     def name(self)->Text:
#         return "action_reset_form_currency_statistic"                
#     async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         slots_names=["tocurrency","fromcurrency","DateCurrencyType","ColumnsCurrencyExchange","intervalduration","elementlist"]
#         Reset_Events=reset_form_slots(slots_names)
#         return Reset_Events                   
        
#endregion


#region Intersting 




#endregion


#region Agency routing 

# class action_routing_agent(Action):
#     def name(self) -> Text:
#         return "action_routing_agent"
#     async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
#             confirm_submit=tracker.get_slot("confirm")
#             if(confirm_submit==True):
#                 dispatcher.utter_message(text="Please hold on, this operation coul take some time .........")
#                 problem_title=tracker.get_slot("problem_title")
#                 dispatcher.utter_message(LiveChat_action=True ,LiveChat_custom_json={"action_name":"agent_routing","sender_id":tracker.sender_id,"problem_tag":problem_title})
#             else:
#                 dispatcher.utter_message(text="Can i help you with some thing else !!!!")
        
#         return []


#endregion


#region Agency location
# class action_Notify_Client(Action):
#     def name (self)->Text:
#         return "action_notify_client"
#     def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         print("notified")
#         dispatcher.utter_message(LiveChat_action=True ,LiveChat_custom_json={"action_name":"notify_location"})
        
#         return[]
# class action_agency_location(Action):
#     def name(self)->Text:
#         return "action_location_response"
    
    
    
#     async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
#         latitude=tracker.get_slot("loactionlatitude")
#         longtitude=tracker.get_slot("locationlongtitude")
#         print(latitude)
#         print(longtitude)
        
#         imp_agency_locations=Imp_Agency_location("")
#         agencies_locations=imp_agency_locations.Get_Agency_locations()
#         addresses=[item.agency_address for item in agencies_locations]
#         cordinates= client_cordinates()
#         cordinates.lat=latitude
#         cordinates.lng=longtitude
    
#         Final_adress=Get_nearest_location(cordinates,addresses)
#         print(Final_adress)
#         if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
#             Rocket_Attachment=convert_data_to_rocketchatAttachment("mapcard",attachment_data=Final_adress)
#             dispatcher.utter_message(text=f"here all the nearest places according to your current position",attachment=Rocket_Attachment)
#         return []
         
     
class  action_refuse_permission(Action):
    def name(self)->Text:
        return "action_refuse_permission"

    
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Please Grant the Location permession ")
        return []
class  action_refuse_permission(Action):
    def name(self)->Text:
        return "action_not_supported"
    
    []
    
    async def run(self, dispatcher:CollectingDispatcher, tracker: Tracker, domain: "DomainDict") -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="you device dosen't  support ")
        return []
             

#endregion 

#region  Fall Back Action
class ActionDefaultFallback(Action):
    def name(self) -> Text:
        return "action_default_fallback"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        if(Is_valide_RocketChat_Channel(tracker.get_latest_input_channel())):
            
            dispatcher.utter_message(text="Do you like to pass to a human ❓ ")
            
            dispatcher.utter_message(text=" You can click Yes 👇 and i will find an agent 🧑‍🔧 for you   ")
            
     
        return [ConversationPaused(), UserUtteranceReverted()]
        
#endregion 

#region Problem routing 



#endregion