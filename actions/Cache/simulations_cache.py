
from typing import List, Text, Union
from rasa_sdk.events import ActiveLoop, SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import REQUESTED_SLOT
from rasa_sdk.interfaces import  Tracker
from ..DAO_Chatbot.Entity.Simulations_entities import Deposit_simulation, simulation, Credit_simulation
from ..DAO_Chatbot.Exceptions.return_exception import Note_found_exception,Chatbot_Error,Empty_list_exception
def Get_selected_simulation(tracker: Tracker, selected_simulation_id: str, list_simulation_slot_name: str, selected_type: str='') -> Union[Credit_simulation,Deposit_simulation,simulation] :
    """
    Get_selected_simulation This function will return the selected Deposit or Credit by the user

    [extended_summary]

    Args:
        tracker (Tracker): [description]
        selected_simulation_id (str): selected id of credit or deposit
        list_simulation_slot_name (str): selected simulation slot name
        selected_type (str, optional): type of simulation 

    Raises:
        Note_found_exception: if the simulation not found
        Empty_list_exception: if the simulation is empty

    Returns:
        Union[Credit_simulation,Deposit_simulation]
    """
    credit_simulation = Credit_simulation()
    deposit_simulation = Deposit_simulation()
    simulations_list= tracker.get_slot(list_simulation_slot_name)
    simutaion_type=tracker.get_slot("selectedsubsimulation") 
    print(simutaion_type)
    if(simulations_list==None):
        raise Note_found_exception("I think the kind of simualtion that you looking for dose not exist any more ",error_type=Chatbot_Error.NOT_FOUND)    
    if(simutaion_type!=None):
        selected_simulations=[simulation for simulation in simulations_list if simulation['simulation_id'] == selected_simulation_id and simulation["Simulation_type"]==simutaion_type]
    else:
        selected_simulations=[simulation for simulation in simulations_list if simulation['simulation_id'] == selected_simulation_id ]

    if(len(selected_simulations)==0):
        raise Empty_list_exception(f"There is no simulation with id equal to {selected_simulation_id}",error_type=Chatbot_Error.NOT_FOUND)
    
    selected_simulation =selected_simulations [0]
    
    
    if str.lower(selected_type) == str.lower('Credit'):
        credit_simulation = Get_basic_simulation(selected_simulation)
        return credit_simulation
    if  str.lower(selected_type) == str.lower('Deposit'):
            deposit_simulation = Get_basic_simulation(selected_simulation)
            deposit_simulation.initial_deposit_min = selected_simulation['initial_deposit_min']
            deposit_simulation.initial_deposit_max = selected_simulation['initial_deposit_max']
            return deposit_simulation
    return Get_basic_simulation(selected_simulation)
    


def Get_basic_simulation(simulation_json: dict) -> simulation:
    simulation_instance = simulation()
    try:
       simulation_instance.simulation_id=          simulation_json['simulation_id']
       simulation_instance.simulation_name=        simulation_json['simulation_name']
       simulation_instance.simulation_details =    simulation_json['simulation_details']
       simulation_instance.periode_max=            simulation_json['periode_max']
       simulation_instance.periode_min=            simulation_json['periode_min']
       simulation_instance.Simulation_type =       simulation_json['Simulation_type']
       simulation_instance.Simulation_added_value= simulation_json['Simulation_added_value']
       simulation_instance.Simulation_max_mounted= float(simulation_json['Simulation_max_mounted'])
       simulation_instance.Simulation_min_mounted= float(simulation_json['Simulation_min_mounted'])
       return simulation_instance

    except  KeyError as error:
       return error 
    except ValueError as error:
        return error
       
def Get_Calcul_Credit_simulation(tracker: Tracker, selected_simulation_id: str, list_simulation_slot_name: str) -> Credit_simulation:
    credit_simulation:Credit_simulation=Get_selected_simulation(tracker, selected_simulation_id, list_simulation_slot_name)
    credit_simulation.mounted_money = float(tracker.get_slot('mountedmoney'))
    credit_simulation.period= int(tracker.get_slot('period'))

    return credit_simulation
def Get_calcul_Deposit_simulation(tracker: Tracker, selected_simulation_id: str, list_simulation_slot_name: str)->Deposit_simulation:
    deposit_simulation:Deposit_simulation=Get_selected_simulation(tracker, selected_simulation_id, list_simulation_slot_name)
    deposit_simulation.mounted_money = float(tracker.get_slot('despositmoney'))
    deposit_simulation.initial_payment=float(tracker.get_slot('initialpayment'))
    deposit_simulation.period=int(tracker.get_slot('perioddeposit'))
    return deposit_simulation

def Get_Current_selected_simulation_name_list(tracker: Tracker) -> Text:
    selected_simulation = tracker.get_slot('selectedsimulation')
    slot_simulation_name = str.lower('simulation_' + selected_simulation + '_list')
    return slot_simulation_name

def Get_all_simulations(tracker: Tracker,simulation_names_list:List[Text])->List[simulation]:
    
    all_simulations:List[simulation]=[tracker.get_slot(simulation_name) for  simulation_name in simulation_names_list ]
    
    return all_simulations
    
def Get_simulations_subtypes(_simulation_type:str,_slot_name:str,_tracker:Tracker)->List[simulation]:
    """
    Get_simulations_subtypes  Retrieving the simulations depends on the type

    Args:
        simulation_type (str): type of simulation For example (Credit For Cars,Credit For Houses...)
        slot_name (str): Target slot name
        tracker (Tracker): Rasa Tracker

    Returns:
        List[simulation]: list of simulations
    """
    if(_simulation_type!=None):
       print(_simulation_type)
       Simulations=[simulation for  simulation in _tracker.get_slot(_slot_name) if simulation["Simulation_type"]==_simulation_type]
       if(len(Simulations)==0):
           raise Empty_list_exception("There is no simulation corresponding to the type of entry")
       else:
           return Simulations
    else :
        Simulations = _tracker.get_slot(_slot_name) 
        return Simulations
  
def cancel_simulation(dispatcher:CollectingDispatcher,tracker:Tracker):
    """
    cancel_simulation this function will cancel the simulation operation

    [extended_summary]

    Args:
        dispatcher (CollectingDispatcher): [description]
        tracker (Tracker): [description]

    Returns:
        [type]: [description]
    """
    if cancel_asking_onSlot_element(dispatcher,tracker):
        return [ActiveLoop(None), SlotSet(REQUESTED_SLOT, None)]  
    else: return []
    
def reset_form_slots(slots_names:List):
    return [SlotSet(slot_name,None) for slot_name in slots_names]

def cancel_asking_onSlot_element(dispatcher:CollectingDispatcher,tracker:Tracker,intent_name:str="stop"):
    """
    cancel_asking_onSlot_element this futions will stop asking on Slot value

    [extended_summary]

    Args:
        dispatcher (CollectingDispatcher): [description]
        tracker (Tracker): [description]
        intent_name (str, optional): [description]. Defaults to "stop". this intent name was declared on Nlu Data

    Returns:
        [type]: [description]
    """
    intent = tracker.latest_message.get("intent", {}).get("name")
    print(intent_name)
    if intent == intent_name:
        print(intent+"++")    
        return True
    return False
