FROM rasa/rasa-sdk:2.4.1

COPY ../actions /app/actions

USER root
RUN pip install --upgrade pip
RUN pip install -r /app/actions/requirements-actions.txt
USER 1001

CMD ["start", "--actions", "actions"]