import json
from pytablewriter import MarkdownTableWriter 


Table_name="Performance" 
intent_report = json.load(open('intent_report.json'))   
gral_values = ['accuracy', 'macro avg', 'weighted avg']
intent_keys = list(intent_report)
intent_keys = [intent for intent in intent_keys if intent not in gral_values] ## Get ALL intent Key Except In the Intent_report
intent_metric = [intent_report[intent] for intent in intent_keys]    


 
metric_keys = list(intent_metric[0].keys())  ##'precision', 'recall', 'f1-score', 'support', 'confused_with' = Global Metric

writer = MarkdownTableWriter()  

writer.table_name = Table_name

writer.headers = ["Intent"]+metric_keys    

writer.margin = 1 
writer.value_matrix = [[intent_keys[index]]+[intent_report[o_key][i_key] for i_key in metric_keys] for index, o_key in enumerate(intent_keys,0)]   
writer.margin = 1 
writer.dump('intent_report.md')
