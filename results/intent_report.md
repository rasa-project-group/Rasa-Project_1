# Performance
|    Intent    | precision  | recall | f1-score | support |          confused_with           |
|--------------|:----------:|-------:|---------:|--------:|----------------------------------|
| deny         | **0.4000** | 0.5714 |   0.4706 |       7 | {'mood_unhappy': 2, 'affirm': 1} |
| greet        | **0.8182** | 0.6923 |   0.7500 |      13 | {'mood_great': 2, 'deny': 2} |
| mood_unhappy | **0.5714** | 0.5714 |   0.5714 |      14 | {'deny': 3, 'mood_great': 2} |
| mood_great   | **0.6875** | 0.7857 |   0.7333 |      14 | {'mood_unhappy': 3} |
| goodbye      | **0.7778** | 0.7000 |   0.7368 |      10 | {'mood_unhappy': 1, 'greet': 1} |
| affirm       | **0.5000** | 0.3333 |   0.4000 |       6 | {'mood_great': 1, 'greet': 1} |
